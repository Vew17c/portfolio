/*
-- Query: 
-- Date: 2018-06-07 12:12
*/
VALUES 
  (1,458658875,'Peter','Andersen','1994-03-21','2002-04-21','2010-04-21',66500.50,'5987 Calloway St','Tallahassee','fl',32324,8505478996,'pandersen@gmail.com',NULL),
  (2,214567889,'Calvin','Wilson','1995-03-21','2002-04-22','2010-04-22',76500.50,'8856 North Monroe St','Tallahassee','fl',32356,8502144447,'cwillson@gmail.com',NULL),
  (3,215469663,'Reese','Hill','1996-03-21','2002-04-23','2010-04-23',54500.50,'0179 Dayflower Circle','Tallahassee','fl',32311,8506327789,'rhill@gmail.com',NULL),
  (4,214578999,'Angel','Wintersen','1997-03-21','2002-04-24','2010-04-24',36500.50,'2654 Thomasville Rd','Tallahassee','fl',32388,8501243658,'awintersen@gmail.com',NULL),
  (5,458741223,'Caroline','Parker','1998-03-21','2002-04-25','2010-04-25',60500.50,'2689 Shamrock Rd','Tallahassee','fl',32302,8507418963,'cparker@gmail.com',NULL);
