-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema vew17c
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `vew17c` ;

-- -----------------------------------------------------
-- Schema vew17c
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `vew17c` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `vew17c` ;

-- -----------------------------------------------------
-- Table `vew17c`.`job`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vew17c`.`job` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vew17c`.`job` (
  `job_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_title` VARCHAR(45) NOT NULL,
  `job_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`job_id`),
  UNIQUE INDEX `job_id_UNIQUE` (`job_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vew17c`.`employee` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vew17c`.`employee` (
  `emp_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_id` TINYINT UNSIGNED NOT NULL,
  `emp_ssn` INT UNSIGNED NOT NULL,
  `emp_fname` VARCHAR(15) NOT NULL,
  `emp_lname` VARCHAR(30) NOT NULL,
  `emp_dob` DATE NOT NULL,
  `emp_start_date` DATE NOT NULL,
  `emp_end_date` DATE NULL,
  `emp_salary` DECIMAL(8,2) UNSIGNED NOT NULL,
  `emp_street` VARCHAR(30) NOT NULL,
  `emp_city` VARCHAR(20) NOT NULL,
  `emp_state` CHAR(2) NOT NULL,
  `emp_zip` INT UNSIGNED NOT NULL,
  `emp_phone` BIGINT UNSIGNED NOT NULL,
  `emp_email` VARCHAR(200) NULL,
  `emp_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`emp_id`),
  UNIQUE INDEX `emp_id_UNIQUE` (`emp_id` ASC),
  UNIQUE INDEX `emp_ssn_UNIQUE` (`emp_ssn` ASC),
  INDEX `fk_employee_job1_idx` (`job_id` ASC),
  CONSTRAINT `fk_employee_job1`
    FOREIGN KEY (`job_id`)
    REFERENCES `vew17c`.`job` (`job_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`benifit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vew17c`.`benifit` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vew17c`.`benifit` (
  `ben_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ben_name` VARCHAR(45) NOT NULL,
  `ben_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`ben_id`),
  UNIQUE INDEX `ben_id_UNIQUE` (`ben_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`plan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vew17c`.`plan` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vew17c`.`plan` (
  `pln_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `ben_id` TINYINT UNSIGNED NOT NULL,
  `pln_type` ENUM('si', 'sp', 'f') NULL,
  `pln_cost` DECIMAL(8,2) NULL,
  `pln_action_date` DATE NULL,
  `pln_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pln_id`),
  UNIQUE INDEX `pln_id_UNIQUE` (`pln_id` ASC),
  INDEX `fk_plan_employee1_idx` (`emp_id` ASC),
  INDEX `fk_plan_benifit1_idx` (`ben_id` ASC),
  CONSTRAINT `fk_plan_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `vew17c`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_plan_benifit1`
    FOREIGN KEY (`ben_id`)
    REFERENCES `vew17c`.`benifit` (`ben_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`emp_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vew17c`.`emp_history` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vew17c`.`emp_history` (
  `eht_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `eht_date` DATE NULL,
  `eht_type` ENUM('s', 'b', 'j') NULL,
  `eht_job_id` TINYINT NULL,
  `eht_emp_salary` DECIMAL(8,2) NULL,
  `eht_user_changed` VARCHAR(45) NULL,
  `eht_reason` VARCHAR(45) NULL,
  `eht_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`eht_id`),
  UNIQUE INDEX `eht_id_UNIQUE` (`eht_id` ASC),
  INDEX `fk_emp_history_employee1_idx` (`emp_id` ASC),
  CONSTRAINT `fk_emp_history_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `vew17c`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`dependent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `vew17c`.`dependent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `vew17c`.`dependent` (
  `dep_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `dep_added` DATE NOT NULL,
  `dep_ssn` INT UNSIGNED NOT NULL,
  `dep_fname` VARCHAR(15) NOT NULL,
  `dep_lname` VARCHAR(30) NOT NULL,
  `dep_dob` DATE NOT NULL,
  `dep_relation` VARCHAR(20) NOT NULL,
  `dep_street` VARCHAR(30) NOT NULL,
  `dep_city` VARCHAR(20) NOT NULL,
  `dep_state` CHAR(2) NOT NULL,
  `dep_zip` INT UNSIGNED NOT NULL,
  `dep_phone` BIGINT UNSIGNED NULL,
  `dep_email` VARCHAR(200) NULL,
  `dep_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`dep_id`),
  UNIQUE INDEX `emp_id_UNIQUE` (`dep_id` ASC),
  UNIQUE INDEX `emp_ssn_UNIQUE` (`dep_ssn` ASC),
  INDEX `fk_dependent_employee_idx` (`emp_id` ASC),
  CONSTRAINT `fk_dependent_employee`
    FOREIGN KEY (`emp_id`)
    REFERENCES `vew17c`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '	';

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `vew17c`.`job`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'secretary', NULL);
INSERT INTO `vew17c`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'service tech', NULL);
INSERT INTO `vew17c`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'manager', NULL);
INSERT INTO `vew17c`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'cashier', NULL);
INSERT INTO `vew17c`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'janitor', NULL);
INSERT INTO `vew17c`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'IT', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 1, 458658875, 'Peter', 'Andersen', '1994-03-21', '2002-04-21', '2010-04-21', 66500.50, '5987 Calloway St', 'Tallahassee', 'fl', 32324, 8505478996, 'pandersen@gmail.com', NULL);
INSERT INTO `vew17c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 2, 214567889, 'Calvin', 'Wilson', '1995-03-21', '2002-04-22', '2010-04-22', 76500.50, '8856 North Monroe St', 'Tallahassee', 'fl', 32356, 8502144447, 'cwillson@gmail.com', NULL);
INSERT INTO `vew17c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 3, 215469663, 'Reese', 'Hill', '1996-03-21', '2002-04-23', '2010-04-23', 54500.50, '0179 Dayflower Circle', 'Tallahassee', 'fl', 32311, 8506327789, 'rhill@gmail.com', NULL);
INSERT INTO `vew17c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 4, 214578999, 'Angel', 'Wintersen', '1997-03-21', '2002-04-24', '2010-04-24', 36500.50, '2654 Thomasville Rd', 'Tallahassee', 'fl', 32388, 8501243658, 'awintersen@gmail.com', NULL);
INSERT INTO `vew17c`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 5, 458741223, 'Caroline', 'Parker', '1998-03-21', '2002-04-25', '2010-04-25', 60500.50, '2689 Shamrock Rd', 'Tallahassee', 'fl', 32302, 8507418963, 'cparker@gmail.com', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`benifit`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`benifit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'medical', NULL);
INSERT INTO `vew17c`.`benifit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'dental', NULL);
INSERT INTO `vew17c`.`benifit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'long-term disability', NULL);
INSERT INTO `vew17c`.`benifit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, '401k', NULL);
INSERT INTO `vew17c`.`benifit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'long-term life insurance', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`plan`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_action_date`, `pln_notes`) VALUES (DEFAULT, 1, 1, 'sp', 350.25, '2002-04-21', NULL);
INSERT INTO `vew17c`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_action_date`, `pln_notes`) VALUES (DEFAULT, 2, 2, 'si', 200.25, '2002-04-22', NULL);
INSERT INTO `vew17c`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_action_date`, `pln_notes`) VALUES (DEFAULT, 3, 3, 'si', 200.25, '2002-04-23', NULL);
INSERT INTO `vew17c`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_action_date`, `pln_notes`) VALUES (DEFAULT, 4, 4, 'f', 670.50, '2002-04-24', NULL);
INSERT INTO `vew17c`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_action_date`, `pln_notes`) VALUES (DEFAULT, 5, 5, 'f', 500.50, '2002-04-25', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`emp_history`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`emp_history` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_user_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 1, '2002-04-21', 's', 1, 56500.50, 'test', 'Recieved a raise', 'Former salary is $66500.50');
INSERT INTO `vew17c`.`emp_history` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_user_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 2, '2002-04-22', 'j', 5, 76500.50, 'test', 'Switch job positions', 'Former job_id is 2');
INSERT INTO `vew17c`.`emp_history` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_user_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 3, '2002-04-23', 'b', 3, 54500.50, 'test', 'Switched benifits', 'Former ben_id is 3');
INSERT INTO `vew17c`.`emp_history` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_user_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 4, '2002-04-24', 'b', 4, 36500.50, 'test', 'Switched benifits', 'Former ben_id is 4');
INSERT INTO `vew17c`.`emp_history` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_user_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 5, '2002-04-25', 'j', 2, 60500.50, NULL, 'Switch job positions', 'Former job_id is 5');

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`dependent`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 1, '2002-04-21', 784521336, 'Carly', 'Andersen', '1994-09-10', 'Spouse', '5987 Calloway St', 'Tallahassee', 'fl', 32380, 8505458799, 'candersen21@gmail.com', NULL);
INSERT INTO `vew17c`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 2, '2002-04-22', 789541225, 'Harry', 'Wilson', '1995-09-10', 'Cousin', '2156 South Monroe St', 'Tallahassee', 'fl', 32366, 8505471123, 'hwilson97@gmail.com', NULL);
INSERT INTO `vew17c`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 3, '2002-04-23', 589647896, 'Peter', 'Hill', '1996-09-10', 'Brother', '1001 Heartwood Hills', 'Tallahassee', 'fl', 32104, 8506711985, 'phill78@gmail.com', NULL);
INSERT INTO `vew17c`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 4, '2002-04-24', 456214588, 'Morgan', 'Wintersen', '1997-09-10', 'Sister', '74458 Winterfield Bd', 'Tallahassee', 'fl', 32301, 8503336589, 'mwintersen61@gmail.com', NULL);
INSERT INTO `vew17c`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 4, '2002-04-24', 200128796, 'Jake', 'Wintersen', '1998-09-10', 'Brother', '78758 Winterfield Bd', 'Tallahassee', 'fl', 32301, 8501478523, 'jwintersen02@gmail.com', NULL);

COMMIT;

