-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema vew17c
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `vew17c` ;

-- -----------------------------------------------------
-- Schema vew17c
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `vew17c` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `vew17c` ;

-- -----------------------------------------------------
-- Table `vew17c`.`person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vew17c`.`person` (
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `per_ssn` BINARY(64) NULL,
  `per_fname` VARCHAR(15) NOT NULL,
  `per_lname` VARCHAR(30) NOT NULL,
  `per_street` VARCHAR(30) NOT NULL,
  `per_city` VARCHAR(30) NOT NULL,
  `per_state` CHAR(2) NOT NULL,
  `per_zip` INT(9) UNSIGNED NOT NULL,
  `per_email` VARCHAR(100) NOT NULL,
  `per_dob` DATE NOT NULL,
  `per_type` ENUM('a', 'c', 'j') NOT NULL,
  `per_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  UNIQUE INDEX `per_id_UNIQUE` (`per_id` ASC),
  UNIQUE INDEX `per_ssn_UNIQUE` (`per_ssn` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`attorney`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vew17c`.`attorney` (
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `aty_start_date` DATE NOT NULL,
  `aty_end_date` DATE NOT NULL,
  `aty_hourly_rate` DECIMAL(5,2) UNSIGNED NOT NULL,
  `aty_years_in_practice` TINYINT UNSIGNED NOT NULL,
  `aty_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  INDEX `fk_attorney_person1_idx` (`per_id` ASC),
  CONSTRAINT `fk_attorney_person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `vew17c`.`person` (`per_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`bar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vew17c`.`bar` (
  `bar_id` TINYINT UNSIGNED NOT NULL,
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `bar_name` VARCHAR(45) NOT NULL,
  `bar_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`bar_id`),
  UNIQUE INDEX `bar_id_UNIQUE` (`bar_id` ASC),
  INDEX `fk_bar_attorney1_idx` (`per_id` ASC),
  CONSTRAINT `fk_bar_attorney1`
    FOREIGN KEY (`per_id`)
    REFERENCES `vew17c`.`attorney` (`per_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`specialty`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vew17c`.`specialty` (
  `spc_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `spc_type` VARCHAR(45) NOT NULL,
  `spc_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`spc_id`),
  UNIQUE INDEX `spc_id_UNIQUE` (`spc_id` ASC),
  INDEX `fk_specialty_attorney1_idx` (`per_id` ASC),
  CONSTRAINT `fk_specialty_attorney1`
    FOREIGN KEY (`per_id`)
    REFERENCES `vew17c`.`attorney` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vew17c`.`client` (
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `cli_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_client_person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `vew17c`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`court`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vew17c`.`court` (
  `crt_id` TINYINT UNSIGNED NOT NULL,
  `crt_name` VARCHAR(45) NOT NULL,
  `crt_street` VARCHAR(30) NOT NULL,
  `crt_city` VARCHAR(30) NOT NULL,
  `crt_state` CHAR(2) NOT NULL,
  `crt_zip` INT(9) UNSIGNED NOT NULL,
  `crt_email` VARCHAR(100) NOT NULL,
  `crt_url` VARCHAR(100) NOT NULL,
  `crt_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`crt_id`),
  UNIQUE INDEX `crt_id_UNIQUE` (`crt_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`judge`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vew17c`.`judge` (
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `crt_id` TINYINT UNSIGNED NOT NULL,
  `jud_salary` DECIMAL(9,2) UNSIGNED NOT NULL,
  `jud_years_in_practice` TINYINT UNSIGNED NOT NULL,
  `jud_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  UNIQUE INDEX `per_id_UNIQUE` (`per_id` ASC),
  INDEX `fk_judge_court1_idx` (`crt_id` ASC),
  CONSTRAINT `fk_judge_person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `vew17c`.`person` (`per_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_judge_court1`
    FOREIGN KEY (`crt_id`)
    REFERENCES `vew17c`.`court` (`crt_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`case`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vew17c`.`case` (
  `cse_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `cse_type` VARCHAR(45) NOT NULL,
  `cse_description` TEXT NOT NULL,
  `cse_start_date` DATE NOT NULL,
  `cse_end_date` DATE NOT NULL,
  `cse_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cse_id`),
  UNIQUE INDEX `cse_id_UNIQUE` (`cse_id` ASC),
  INDEX `fk_case_judge1_idx` (`per_id` ASC),
  CONSTRAINT `fk_case_judge1`
    FOREIGN KEY (`per_id`)
    REFERENCES `vew17c`.`judge` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`assignment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vew17c`.`assignment` (
  `asn_id` SMALLINT UNSIGNED NOT NULL,
  `per_cid` SMALLINT UNSIGNED NOT NULL,
  `per_aid` SMALLINT UNSIGNED NOT NULL,
  `cse_id` SMALLINT UNSIGNED NOT NULL,
  `asn_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`asn_id`),
  INDEX `fk_assignment_client1_idx` (`per_cid` ASC),
  INDEX `fk_assignment_attorney1_idx` (`per_aid` ASC),
  INDEX `fk_assignment_case1_idx` (`cse_id` ASC),
  CONSTRAINT `fk_assignment_client1`
    FOREIGN KEY (`per_cid`)
    REFERENCES `vew17c`.`client` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_assignment_attorney1`
    FOREIGN KEY (`per_aid`)
    REFERENCES `vew17c`.`attorney` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_assignment_case1`
    FOREIGN KEY (`cse_id`)
    REFERENCES `vew17c`.`case` (`cse_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`judge_hist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vew17c`.`judge_hist` (
  `jhs_id` SMALLINT UNSIGNED NOT NULL,
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `jhs_crt_id` TINYINT UNSIGNED NOT NULL,
  `jhs_date` TIMESTAMP NOT NULL,
  `jhs_type` ENUM('i', 'u', 'd') NOT NULL,
  `jhs_salary` DECIMAL(8,2) UNSIGNED NOT NULL,
  `jhs_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`jhs_id`),
  UNIQUE INDEX `jhs_id_UNIQUE` (`jhs_id` ASC),
  INDEX `fk_judge_hist_judge1_idx` (`per_id` ASC),
  CONSTRAINT `fk_judge_hist_judge1`
    FOREIGN KEY (`per_id`)
    REFERENCES `vew17c`.`judge` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `vew17c`.`phone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vew17c`.`phone` (
  `phn_id` SMALLINT UNSIGNED NOT NULL,
  `per_id` SMALLINT UNSIGNED NOT NULL,
  `phn_num` BIGINT UNSIGNED NOT NULL,
  `phn_type` ENUM('home', 'office', 'cell') NOT NULL,
  `phn_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`phn_id`),
  UNIQUE INDEX `phn_id_UNIQUE` (`phn_id` ASC),
  INDEX `fk_phone_person_idx` (`per_id` ASC),
  CONSTRAINT `fk_phone_person`
    FOREIGN KEY (`per_id`)
    REFERENCES `vew17c`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `vew17c`.`person`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (1, NULL, 'Wilber', 'Fische', '5749 Dayflower Circle', 'Tallahassee', 'FL', 32310, 'wfische@gmail.com', '1984-04-07', 'c', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (2, NULL, 'John', 'Travolta', '7906 Bradford Street', 'Tallahassee', 'FL', 32311, 'jtravolta@gmail.com', '1985-04-07', 'c', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (3, NULL, 'Andy', 'Griffin', '782 Spruce Street ', 'Tallahassee', 'FL', 32312, 'agriffin@gmail.com', '1986-04-07', 'c', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (4, NULL, 'Luke', 'Skywalker', '8906 Center Ave. ', 'Tallahassee', 'FL', 32313, 'lskywalker@gmail.com', '1987-04-07', 'c', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (5, NULL, 'Andrew', 'Jackson', '11 Princess Ave. ', 'Tallahassee', 'FL', 32314, 'ajackson@gmail.com', '1988-04-07', 'c', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (6, NULL, 'Taylor', 'Hacker', '347 Lakeshore Rd. ', 'Tallahassee', 'FL', 32315, 'thacker@gmail.com', '1989-04-07', 'a', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (7, NULL, 'Connor', 'McGiven', '54 Clay Drive ', 'Tallahassee', 'FL', 32316, 'cmcgiven@gmail.com', '1990-04-07', 'a', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (8, NULL, 'Evan', 'Powell', '86 NE. Clay Street ', 'Tallahassee', 'FL', 32317, 'epowell@gmail.com', '1991-04-07', 'a', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (9, NULL, 'Vincent', 'Willson', '310 Proctor St. ', 'Tallahassee', 'FL', 32318, 'vwillson@gmail.com', '1992-04-07', 'a', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (10, NULL, 'Jeffery', 'Pattersen', '67 Penn Dr. ', 'Tallahassee', 'FL', 32319, 'jpattersen@gmail.com', '1993-04-07', 'a', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (11, NULL, 'Noah', 'Ark', '9424 Cemetery Road ', 'Tallahassee', 'FL', 32320, 'nark@gmail.com', '1994-04-07', 'j', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (12, NULL, 'Stephane', 'Fetchen', '4 Homestead Ave. ', 'Tallahassee', 'FL', 32321, 'sfetchen@gmail.com', '1995-04-07', 'j', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (13, NULL, 'Shannon', 'Hughes', '885 Franklin Ave. ', 'Tallahassee', 'FL', 32322, 'shughes@gmail.com', '1996-04-07', 'j', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (14, NULL, 'Blake', 'Reese', '7469 Jones Road ', 'Tallahassee', 'FL', 32323, 'breese@gmail.com', '1997-04-07', 'j', NULL);
INSERT INTO `vew17c`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (15, NULL, 'Trevor', 'Reese', '62 West Garfield Dr. ', 'Tallahassee', 'FL', 32323, 'treese@gmail.com', '1998-04-07', 'j', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`attorney`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (6, '2001-01-19', '2001-05-19', 127.68, 5, NULL);
INSERT INTO `vew17c`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (7, '2002-01-19', '2002-05-19', 100.75, 4, NULL);
INSERT INTO `vew17c`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (8, '2003-01-19', '2003-05-19', 98.25, 5, NULL);
INSERT INTO `vew17c`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (9, '2004-01-19', '2004-05-19', 147.69, 6, NULL);
INSERT INTO `vew17c`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (10, '2005-01-19', '2005-05-19', 168.2, 3, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`bar`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (1, 6, 'Blue', NULL);
INSERT INTO `vew17c`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (2, 7, 'Green', NULL);
INSERT INTO `vew17c`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (3, 8, 'Yellow', NULL);
INSERT INTO `vew17c`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (4, 9, 'Red', NULL);
INSERT INTO `vew17c`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (5, 10, 'Black', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`specialty`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (1, 6, 'Procecution', NULL);
INSERT INTO `vew17c`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (2, 7, 'Defense', NULL);
INSERT INTO `vew17c`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (3, 8, 'Defense', NULL);
INSERT INTO `vew17c`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (4, 9, 'Prosecutor', NULL);
INSERT INTO `vew17c`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (5, 10, 'Investegator', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`client`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`client` (`per_id`, `cli_notes`) VALUES (1, NULL);
INSERT INTO `vew17c`.`client` (`per_id`, `cli_notes`) VALUES (2, NULL);
INSERT INTO `vew17c`.`client` (`per_id`, `cli_notes`) VALUES (3, NULL);
INSERT INTO `vew17c`.`client` (`per_id`, `cli_notes`) VALUES (4, NULL);
INSERT INTO `vew17c`.`client` (`per_id`, `cli_notes`) VALUES (5, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`court`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_email`, `crt_url`, `crt_notes`) VALUES (1, 'Supreme Court', '8962 N.Monroe St.', 'Tallahassee', 'FL', 32311, 'scourt@fl.gov', 'www.supreme_court.gov', NULL);
INSERT INTO `vew17c`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_email`, `crt_url`, `crt_notes`) VALUES (2, 'District Court of Appeal', '8962 N.Monroe St.', 'Tallahassee', 'FL', 32311, 'dcourt@fl.gov', 'www.distictcourt.gov', NULL);
INSERT INTO `vew17c`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_email`, `crt_url`, `crt_notes`) VALUES (3, 'District Court Clerk', '8962 N.Monroe St.', 'Tallahassee', 'FL', 32311, 'dccourt@fl.gov', 'www.fldistrictclerk.gov', NULL);
INSERT INTO `vew17c`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_email`, `crt_url`, `crt_notes`) VALUES (4, 'Leon County Court', '8962 N.Monroe St.', 'Tallahassee', 'FL', 32311, 'lccourt@fl.gov', 'www.leoncountycourt.gov', NULL);
INSERT INTO `vew17c`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_email`, `crt_url`, `crt_notes`) VALUES (5, 'Bankruptcy Count Court', '8962 N.Monroe St.', 'Tallahassee', 'FL', 32311, 'bcourt@fl.gov', 'www.backruptgov.gov', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`judge`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (11, 1, 78954.98, 5, NULL);
INSERT INTO `vew17c`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (12, 2, 21456.96, 2, NULL);
INSERT INTO `vew17c`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (13, 3, 21365.78, 7, NULL);
INSERT INTO `vew17c`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (14, 4, 11023.96, 8, NULL);
INSERT INTO `vew17c`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (15, 5, 78963.21, 9, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`case`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (1, 11, 'Misdemeanor', 'Broke into someone\'s house', '2000-01-19', '2000-05-19', NULL);
INSERT INTO `vew17c`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (2, 12, 'Arson', 'Burned down ones own house', '2001-01-19', '2001-05-19', NULL);
INSERT INTO `vew17c`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (3, 13, 'Murder', 'Killed a person', '2002-01-19', '2002-05-19', NULL);
INSERT INTO `vew17c`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (4, 14, 'Grand Theft Auto', 'Bumblebee is stolen', '2003-01-19', '2003-05-19', NULL);
INSERT INTO `vew17c`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (5, 15, 'Grand Theft', '$100000 was stolen', '2004-01-19', '2004-05-19', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`assignment`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (11, 1, 6, 1, NULL);
INSERT INTO `vew17c`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (12, 2, 7, 2, NULL);
INSERT INTO `vew17c`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (13, 3, 8, 3, NULL);
INSERT INTO `vew17c`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (14, 4, 9, 4, NULL);
INSERT INTO `vew17c`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (15, 5, 10, 5, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`judge_hist`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (1, 11, 1, '2001-01-19', 'i', 82145.68, NULL);
INSERT INTO `vew17c`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (2, 12, 2, '2002-01-19', 'i', 12478.69, NULL);
INSERT INTO `vew17c`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (3, 13, 3, '2003-01-19', 'u', 12547.68, NULL);
INSERT INTO `vew17c`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (4, 14, 4, '2004-01-19', 'u', 85625.69, NULL);
INSERT INTO `vew17c`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (5, 15, 5, '2005-01-19', 'd', 15476.69, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `vew17c`.`phone`
-- -----------------------------------------------------
START TRANSACTION;
USE `vew17c`;
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (1, 1, 8505458951, 'home', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (2, 2, 8501425655, 'cell', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (3, 3, 8501474523, 'office', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (4, 4, 8505459632, 'office', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (5, 5, 8501254692, 'home', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (6, 6, 8503214569, 'cell', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (7, 7, 8504577718, 'cell', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (8, 8, 8509632154, 'home', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (9, 9, 8504569632, 'office', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (10, 10, 8501245879, 'office', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (11, 11, 8505412365, 'office', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (12, 12, 8504789632, 'home', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (13, 13, 8505421236, 'cell', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (14, 14, 8504712365, 'home', NULL);
INSERT INTO `vew17c`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (15, 15, 8501247852, 'home', NULL);

COMMIT;

