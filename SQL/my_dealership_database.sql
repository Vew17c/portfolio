
DROP DATABASE IF EXISTS vew17c;
CREATE DATABASE IF NOT EXISTS vew17c;

USE vew17c;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer (
  cus_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  srp_id SMALLINT UNSIGNED NOT NULL,
  cus_ssn INT(9) UNSIGNED ZEROFILL NOT NULL,
  cus_fname VARCHAR(15) NOT NULL,
  cus_lname VARCHAR(30) NOT NULL,
  cus_street VARCHAR(30) NOT NULL,
  cus_city VARCHAR(20) NOT NULL,
  cus_state CHAR(2) NOT NULL,
  cus_zip INT UNSIGNED NOT NULL,
  cus_phone BIGINT UNSIGNED NOT NULL,
  cus_email VARCHAR(100) NULL,
  cus_balance DECIMAL(8,2) UNSIGNED NOT NULL,
  cus_tot_sales DECIMAL(10,2) UNSIGNED NOT NULL,
  cus_notes VARCHAR(255) NULL,
  PRIMARY KEY (cus_id),
  INDEX idx_srp_id (srp_id ASC),
  INDEX idx_cus_ssn (cus_ssn ASC),
  INDEX idx_cus_lname (cus_lname ASC),
  CONSTRAINT fk_slsrep_customer
   FOREIGN KEY (srp_id)
   REFERENCES slsrep (srp_id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT
);
 
DROP TABLE IF EXISTS dealership;
CREATE TABLE IF NOT EXISTS dealership (
  dlr_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  dlr_name VARCHAR(30) NOT NULL,
  dlr_street VARCHAR(30) NOT NULL,
  dlr_city VARCHAR(20) NOT NULL,
  dlr_state CHAR(2) NOT NULL,
  dlr_zip INT(9) ZEROFILL UNSIGNED NOT NULL,
  dlr_phone BIGINT UNSIGNED NOT NULL,
  dlr_ytd_sales DECIMAL(8,2) UNSIGNED NOT NULL,
  dlr_email VARCHAR(100) NOT NULL DEFAULT 'info@mydealership.com',
  dlr_url VARCHAR(200) NULL,
  dlr_notes VARCHAR(255) NULL,
  PRIMARY KEY (dlr_id),
  INDEX idx_dlr_name (dlr_name ASC)
);

DROP TABLE IF EXISTS slsrep;
CREATE TABLE IF NOT EXISTS slsrep (
  srp_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  dlr_id SMALLINT UNSIGNED NOT NULL,
  srp_ssn INT(9) UNSIGNED ZEROFILL NOT NULL,
  srp_fname VARCHAR(15) NOT NULL,
  srp_lname VARCHAR(30) NOT NULL,
  srp_dob DATE NOT NULL,
  srp_street VARCHAR(30) NOT NULL,
  srp_city VARCHAR(20) NOT NULL,
  srp_state CHAR(2) NOT NULL,
  srp_zip INT UNSIGNED NOT NULL,
  srp_phone BIGINT UNSIGNED NOT NULL,
  srp_email VARCHAR(100) NULL,
  srp_tot_sales DECIMAL(10,2) UNSIGNED NOT NULL,
  srp_comm DECIMAL(7,2) UNSIGNED NOT NULL,
  srp_notes VARCHAR(255) NULL,
  PRIMARY KEY (srp_id),
  INDEX idx_dlr_id (dlr_id ASC),
  INDEX idx_srp_ssn (srp_ssn ASC),
  INDEX idx_srp_lname (srp_lname ASC),
  CONSTRAINT fk_dealership_slsrep
   FOREIGN KEY (dlr_id)
   REFERENCES dealership (dlr_id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT
);

DROP TABLE IF EXISTS vehicle;
CREATE TABLE vehicle(
  veh_vin CHAR(17) NOT NULL,
  dlr_id SMALLINT UNSIGNED NOT NULL,
  cus_id SMALLINT UNSIGNED NOT NULL,
  veh_type VARCHAR(5) NOT NULL DEFAULT 'auto', 
  veh_make VARCHAR(20) NOT NULL,
  veh_model VARCHAR(25) NOT NULL,
  veh_year YEAR(4) NOT NULL,
  veh_price DECIMAL(8,2) UNSIGNED NOT NULL,
  veh_notes VARCHAR(255) NULL,
  PRIMARY KEY (veh_vin),
  INDEX idx_dlr_id (dlr_id ASC),
  INDEX idx_cus_id (cus_id ASC),
  CONSTRAINT fk_dealership_vehicle
   FOREIGN KEY (dlr_id)
   REFERENCES dealership (dlr_id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT,
  CONSTRAINT fk_customer_vehicle
   FOREIGN KEY (cus_id)
   REFERENCES customer (cus_id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT
);

-- CREATE UNIQUE INDEX ux_cus_ssn on customer(cus_ssn);

SET FOREIGN_KEY_CHECKS=1;

INSERT INTO dealership (dlr_name, dlr_street, dlr_city, dlr_state, dlr_zip, dlr_phone, dlr_ytd_sales, dlr_url, dlr_notes)
  VALUES 
  ('Claude Short Dodge','1115 Easterwood Dr.','tallahassee','fl',32311,8508786180,85069.59,'www.americancross.com',NULL),
  ('Merolis Chevrolet','10485 History Rd.','tallahassee','fl',32311,8502786989,85069.59,'www.atlus.com',NULL),
  ('Beaverton Chrysler','11000 Tung Grove Rd.','tallahassee','fl',32311,8503786687,85069.59,'www.aniplex.com',NULL),
  ('Dowd Ford','8607 Highlander Dr.','tallahassee','fl',32311,8505786682,85069.59,'www.bushiroadna.com',NULL),
  ('Seattle Toyota','10002 Easterwood Dr','tallahassee','fl',32311,8509786381,85069.59,'www.gumius.com',NULL);

INSERT INTO slsrep (dlr_id, srp_ssn, srp_fname, srp_lname, srp_dob, srp_street, srp_city, srp_state, srp_zip, srp_phone, srp_email, srp_tot_sales, srp_comm)
  VALUES 
  (1, 123456789, 'Charlie', 'Sheen','1948-12-01','173 Harbor Way','tallahassee','fl',102897671,8501234567,'csheen@gmail.com',256678.00,987.30),
  (2, 921436082, 'Monroe', 'Jackson','1945-12-02','183 Harbor Way','tallahassee','fl',003895673,8509233583,'mjackson@gmail.com',358678.00,587.30),
  (3, 822466183, 'George', 'Callaway','1944-12-03','193 Harbor Way','tallahassee','fl',104896674,8505235542,'gcallaway@gmail.com',452678.00,1087.30),
  (4, 724486084, 'Marie', 'Andersen','1943-12-04','103 Harbor Way','tallahassee','fl',005898675,8504236516,'mandersen@gmail.com',559678.00,887.30),
  (5, 625496185, 'Casey', 'Walker','1942-12-05','113 Harbor Way','tallahassee','fl',106899676,8503237511,'cwalker@gmail.com',151678.00,687.30);

INSERT INTO customer (srp_id, cus_ssn, cus_fname, cus_lname, cus_street, cus_city, cus_state, cus_zip, cus_phone, cus_email, cus_balance, cus_tot_sales, cus_notes)
  VALUES  
  (1,458658875,'Peter','Andersen','5987 Calloway St','tallahassee','fl',32324,8505478996,'pandersen@gmail.com',5000.00,37000.00,NULL),
  (2,214567889,'Calvin','Wilson','8856 North Monroe St','tallahassee','fl',32356,8502144447,'cwillson@gmail.com',5000.00,37000.00,NULL),
  (3,215469663,'Reese','Hill','0179 Dayflower Circle','tallahassee','fl',32311,8506327789,'rhill@gmail.com',5000.00,37000.00,NULL),
  (4,214578999,'Angel','Wintersen','2654 Thomasville Rd','tallahassee','fl',32388,8501243658,'awintersen@gmail.com',5000.00,37000.00,NULL),
  (5,458741223,'Caroline','Parker','2689 Shamrock Rd','tallahassee','fl',32302,8507418963,'cparker@gmail.com',5000.00,37000.00,NULL);

INSERT INTO vehicle (veh_vin, dlr_id, cus_id, veh_type, veh_make, veh_model, veh_year, veh_price, veh_notes)
  VALUES 
  ('1FTCR15X0TTA01050', 1, 5, 'Truck', 'Ford', 'Ranger', 2010, 53995.00, 'Heavy Duty'),
  ('2A9B2C9D5E6F2G8H0', 2, 4, 'Auto', 'Chevrolet', 'Corvette', 2008, 30052.00, 'Viper Killer'),
  ('3A8B3AED5E6F2G840', 3, 2, 'SUV', 'Jeep', 'Liberty', 2009, 68254.00, 'Off-road vehicle'),
  ('4A7B4QWW5E6F3G8H0', 4, 1, 'Auto', 'Suburu', 'Outback XT', 2011, 53689.00, 'Utility'),
  ('5A6B5C6D5E6F3G840', 5, 3, 'Auto', 'Dodge', 'Viper', 2012, 85000.00, 'Super fast!');

-- TEE C:\Users\Darkj\Desktop\Tee_Output\a3_report_vincnet.txt

SELECT * FROM customer;
SELECT * FROM dealership;
SELECT * FROM slsrep;
SELECT * FROM vehicle;

-- NOTEE


