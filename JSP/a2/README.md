> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advance Web Application Development

## Vincent Williams

### Assignment 2 Requirements:

Assessment: the following links should properly display (see screenshots below): 

1. http://localhost:9999/hello (displays directory, needs index.html) 

2. http://localhost:9999/hello/HelloHome.html (Rename "HelloHome.html" to "index.html") 

3. http://localhost:9999/hello/sayhello (invokes HelloServlet) Note: /sayhello maps to HelloServlet.class (changed web.xml file) 

4. http://localhost:9999/hello/querybook.html 

5. http://localhost:9999/hello/sayhi (invokes AnotherHelloServlet)

README.md file should include the following items:        

* Screenshot Tomcat WepApp
* Screenshot of running Servlets
* Screenshot of Servlets acessing a database
                                                                                                                    
Assignment Screenshots: 

*Screenshot of localhost:9999/hello*:

![JDK Installation Screenshot](../img/cat_1.png)

*Screenshot of localhost:9999/hello/index.html*:

![JDK Installation Screenshot](../img/cat_2.png)

*Screenshot of localhost:9999/hello/sayhello:

![JDK Installation Screenshot](../img/cat_3.png)

*Screenshot of localhost:9999/hello/querybook.html*:

![JDK Installation Screenshot](../img/cat_4.png)

*Screenshot of localhost:9999/hello/sayhi*:

![JDK Installation Screenshot](../img/cat_5.png)

*Screenshot of a selected item in localhost:9999/hello/querybook.html*:

![JDK Installation Screenshot](../img/cat_6.png)

*Screenshot of query results*:

![JDK Installation Screenshot](../img/cat_7.png)