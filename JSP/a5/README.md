> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advance Web Application Development

## Vincent Williams

### Assignment 5 Requirements:

1. Include client side validation
2. Create server side validation
3. Learn about the use of Model Controller View
4. Use JavaBeans for server side validation
5. Learn how to connect an app to a database


README.md file should include the following items:        

1. Course title, your name, assignment requirements, as per A1
2. Screenshot of running application on A5 Page
3. Screenshot of passed validation.       
4. Screenshot of associated database.

### Assignment Screenshots:  

*Screenshot of valid user form entry*:
![Valid User Form Entry](../img/a5_screen_1.png) 

*Screenshot of passed validation*:
![Passed Validation](../img/a5_screen_2.png) 

*Screenshot of associated database*:
![Database](../img/a5_screen_3.png) 


