> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advance Web Applications Development

## Vincent Williams

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Installing Java
    - Install and Starting up Tomcat
    - Learning about Git Hub and some commands for Git HUb
    - Creating a "Hello World" Java program.

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Setting up / configuring Tomcat
    - Developing a WebApp
    - Creating servlets for the wepage to read
    - Establishing a database connection and having the servlet read infromation from it
    - Deploying Servlet using @WebServlet  

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Creating a Relational database
    - Learning about primary - foreign key relationships
    - Setting commands to the foreign keys when the foreign key is updated or removed

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Include client side validation
    - Create server side validation
    - Learn about the use of Model Controller View
    - Use JavaBeans for server side validation

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Include server-side validation
    - Learn how to connect a web app to a database
    - Application must be able to populate a database
    - Learn about java.sql tools 

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a working carousel that redirects the user when clicekd on
    - Create Client Side Validation
    - Learn how regular expressions work

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Complete CRUD functionality
    - Have the user be able to update their records and have the results shown on screen.
    - GIve the user the ability to delete their records and have a prompt show up when they are about to delete a record. 


