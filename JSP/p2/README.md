> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advance Web Application Development

## Vincent Williams

### Project 2 Requirements:

1. Include client side validation
2. Create server side validation
3. Complete the CRUD functionality 
4. Must be able to update the database
5. Must be able to delete a record


README.md file should include the following items:        

1. Course title, your name, assignment requirements
2. Screenshot of running application on P2 page
3. Screenshot of passed validation.       
4. Screenshot of new record
5. Screenshot of the update page
6. Screenshot of an editted record
7. Screenshot of the a record being deleted from the database

### Assignment Screenshots:  

*Screenshot of valid user form entry*:
![Valid User Form Entry](../img/p2_1.png) 

*Screenshot of thank you page*:
![Valid User Form Entry](../img/p2_2.png) 

*Screenshot of table showing on the app*:
![Valid User Form Entry](../img/p2_3.png) 

*Screenshot of update record page*:
![Valid User Form Entry](../img/p2_4.png) 

*Screenshot of updated record*:
![Valid User Form Entry](../img/p2_5.png) 

*Screenshot of delete confirmation page*:
![Valid User Form Entry](../img/P2_6.png) 

*Screenshot of altered database*:
![Valid User Form Entry](../img/P2_7.png) 