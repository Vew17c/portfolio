> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Vincent Williams

### LIS4381 Requirements:

*Course Work Links*

1) [A1_README.md](https://bitbucket.org/Vew17c/lis4381/src/master/a1/A1_README.md)

* Setting up Java
* Setting up Android Studiodd
* Setting up Ammps
* Setting up Bitbucket    

2) [A2_README.md](https://bitbucket.org/Vew17c/lis4381/src/master/a2/A2_README.md)

* Creating a app that gives us the recipe to Brushetta
* Demonstrates the use of activites and buttons

3) [A3_README.md](https://bitbucket.org/Vew17c/lis4381/src/master/a3/A3_README.md)

* Created an app that calculates the price of the tickets (acording to the ticket price and how many tickets you are getting).
* Demostrates the usage of java in AndroidStudios
* Created a small database in MySQL WOrkbench

4) [A4_README.md](https://bitbucket.org/Vew17c/lis4381/src/master/a4/A4_README.md)

* Setting up a webpage to accept user input
* Setting up fro Client-Side Validation

5) [A5_README.md](https://bitbucket.org/Vew17c/lis4381/src/master/a5/A5_README.md)

* Acressing data from a database and showing it on the website
* Setting up server-side validation.
* Creating a workable interface where a user can enter data and it updates the database.

6) [P1_README.md](https://bitbucket.org/Vew17c/lis4381/src/master/p1/P1_README.md)

* Created an app that can be used as my buisness card.
* The app itself uses 2 activities.

7) [P2_README.md](https://bitbucket.org/Vew17c/lis4381/src/master/p2/P2_README.md)

* Acressing data from a database and showing it on the website
* Setting up server-side validation.
* Be able to edit records.
* Be able to delete records.

