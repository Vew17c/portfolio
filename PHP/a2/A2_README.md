> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Vincent Williams

### Assignment 2 Requirements:

Create an app using Android Studios

#### README.md file should include the following items:

* Screenshot of the App's home screen
* Screenshot of the App's recipe page.

#### Assignment Screenshots:

*Screenshot of the app's Home page*:

![App home page](img/app_screen1.png)

*Screenshot of Bruschetta recipe*:

![Recipe page](img/app_screen2.png)