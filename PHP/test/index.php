<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Your Name Here!">
	<link rel="icon" href="../img/favicon.ico">

	<title>LIS4381 - Assignment4</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>
<?php include_once("global/nav.php"); ?>
	
	<?php 
$result = "";
class calculator
{
    var $a;
	var $b;
	var $result = "";

    function checkopration($oprator)
    {
        switch($oprator)
        {
            case '+':
			//return $this->a + $this->b;
			echo "$a"."+"."$b"."=";
			echo "$a + $b";
            break;

            case '-':
            return $this->a - $this->b;
            break;

            case '*':
            return $this->a * $this->b;
            break;

            case '/':
            if ($this->b == 0 ){
				return "Cannot be divided by 0";
			}
            break;
            
            return $this->a / $this->b;
            break;

            case 'exp':
            return pow($this->a, $this->b);
            break;

            default:
            return "Sorry No command found";
        }   
    }

    function getresult($a, $b, $c)
    {
        $this->a = $a;
        $this->b = $b;
        return $this->checkopration($c);
    }
}

$cal = new calculator();
if(isset($_POST['submit']))
{   
    $result = $cal->getresult($_POST['n1'],$_POST['n2'],$_POST['op']);
}
?>

<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>Preform Calculation</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="#">
								<div class="form-group">
										<label class="col-sm-4 control-label">Number 1:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="n1" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Number 2:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="n2" />
										</div>
								</div>

								<input type="radio" name="op" id="+" value="+" checked="true"> Addition &nbsp;&nbsp;
								<input type="radio" name="op" id="-" value="-"> Subtraction &nbsp;&nbsp;
								<input type="radio" name="op" id="*" value="*"> Multiplcation &nbsp;&nbsp;
								<input type="radio" name="op" id="/" value="/"> Division &nbsp;&nbsp;
								<input type="radio" name="op" id="exp" value="exp"> Exponetiate </p>

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="submit" value="submit" onclick="calNumber()">Calculate</button>
										</div>
								</div>

								<p><p><td><strong><?php echo $result; ?><strong></td><p><P>
						</form>
			
			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

</body>
</html>