> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Vincent Williams

### Project 1 Requirements:

1. Screenshot of the running application for the first user interface
2. Screenshot of the running application for the first user interface


#### Assignment Screenshots:

*First user interface*:

![First Interface](img/p1_1.png)

*Sirst user interface*:

![Second Interface](img/p1_2.png)
