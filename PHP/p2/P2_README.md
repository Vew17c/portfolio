> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Vincent Williams

### Project 2 Requirements:

* Set up our online portfolio
* Set up our A5 page and getting the edit and delete function to work
* Add Server-side validation to the interfaces.
* Using RSS

#### README.md file should include the following items:

* Screenshot of P2's functioning interface
* Screenshot of error page on edit_petstore_processes.php
* Screenshot of Carousel
* Screenshot of RSS feed.

#### Assignment Screenshots:

*Screenshot of P2 :

![P2 Page](img/p2_1.png)

*Screenshots of error on edit_petstore.php:

![Edit Page](img/p2_2.png)

*Screenshots of the Serverside Error:

![Error](img/p2_3.png)

*Screenshots of Carousel:

![Carousel](img/p2_4.png)

*Screenshot of RSS feed:

![RSS](img/p2_5.png)
