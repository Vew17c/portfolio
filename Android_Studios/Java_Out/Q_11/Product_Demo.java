import java.util.Scanner;
import java.text.DecimalFormat;

class Product_Demo {
    public static void main(String[] args){
        
        String codeI = "",
               descriptI = "",
               priceI = "";
        
        Scanner input = new Scanner(System.in);
        DecimalFormat f = new DecimalFormat("###,###.##");

        System.out.println("/// Below are the user-entered values: /////");

        Product pro = new Product();

        System.out.println("\nCode = " + pro.getCode());
        System.out.println("\nDescription = " + pro.getDescription());
        System.out.println("\nPrice = $" + pro.getPrice());

        System.out.println("\n///// Blow are user-entered b=values: ////");

        System.out.print("Code: ");
        codeI = input.next();
        System.out.print("Description: ");
        descriptI = input.next();
        System.out.print("Price: ");
        priceI = input.next();

        Product pro2 = new Product(codeI, descriptI, priceI);

        System.out.println("\nCode = " + pro2.getCode());
        System.out.println("\nDescription = " + pro2.getDescription());
        System.out.println("\nPrice = $" + pro2.getPrice());

        System.out.println("\n//// Below using setter methoods");
        pro2.setCode("xyz789");
        pro2.setDescription("Test Widget");
        pro2.setPrice("89.99");
        pro2.print();
    }
}