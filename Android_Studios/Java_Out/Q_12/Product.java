class Product {
    public String code;
    public String description;
    public String price;

    public Product() {
        System.out.println("\nInside product default constructors.");
        code = "abc123";
        description = "My widget";
        price = "49.99";
    }

    public Product( String code, String description, String price) {
        System.out.println("\nInside product constructor with parameters.");
        this.code = code;
        this.description = description;
        this.price = price;
    }

    public String getCode(){
        return code;
    }

    public String getDescription (){
        return description;
    }

    public String getPrice (){
        return price;
    }

    public void setCode( String a ){
        code = a;
    }

    public void setDescription ( String d ){
        description = d;
    }

    public void setPrice ( String p ){
        price = p;
    }

    public void print() {
        System.out.print("\nCode:" + code + ", Description: " + description + ", Price: $" + price);
    }

}