> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advance Mobile Application Development

## Vincent Williams

### Assignment 1 Requirements:

1. Distributed Control Setup
2. Development Instillations
3. Questions

| README.md file should include the following items:  | Git commands w/short descriptions:
|:--                                                  |:--
| * Screenshot of Android Studio's "MyBuisnessCard"   | 1. git init: Create an empty Git repository or reinitialize an existing one.
| * Screenshot of Java "Hello World"                  | 2. git status: Show the working tree status.
| * Screenshot of Android Studio's "My first App"     | 3. git add: Add file contents to the index.
| * Git commands                                      | 4. git commit: Saves the changes to the repository.
| * Bitbucket repo links                              | 5. git push: Updates remote refs along with associated objects.
|                                                     | 6. git pull: Fetch from / integrate with another repository / a local branch.
|                                                     | 7. git config: Configures the author's name and email to your commits.

|Assignment Screenshots: |
|:-- |:--
| *Screenshot of running java Hello*: | *Screenshot of Android Studio - My First App*:
| |
| ![JDK Installation Screenshot](../img/jdk_install.png) | ![Android Studio Installation Screenshot](../img/android.png)
| |
| *Screenshot of Android Studio - MyBuisnessCard*: | *Screenshot of Android Studio - Contact Page*:
| |
| ![My Buisness Card Screenshot](../img/Screen2.png) | ![Contact Page Screenshot](../img/Screen1.png)












#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Vew17c/lis4331/src/master/bitbucketstationlocations.html)
