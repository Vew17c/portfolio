> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advance Mobile Application Development

## Vincent Williams

### Assignment 4 Requirements:

1. Include splash screen image, app title, intro text. 
2. Include appropriate images. 
3. Must use persistent data: SharedPreferences 
4. Widgets and images must be vertically and horizontally aligned. 
5. Must add background color(s) or theme 
6. Create and display launcher icon image 

README.md file should include the following items:        

1. Course title, your name, assignment requirements, as per A1
2. Screenshot of running applications homepage
3. Screenshot of running applications invalid input
4. Screenshot of running applications valid input
                                                                                                                           

|Assignment Screenshots: | | 
|:-- |:-- |:--
| *Screenshot of Mortgage Intrest App*: | *Screenshot of invalid Input*: | *Screenshot of valid input*:
| | | 
| ![Screenshot of the Mortgage Intrest App](../img/a4_screen_1.png) | ![Screenshot of invalid input in the app](../img/a4_screen_2.png) | ![Screenshot of valid input in the app](../img/a4_screen_3.png) |
|

