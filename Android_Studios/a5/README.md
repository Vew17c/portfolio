> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advance Mobile Application Development

## Vincent Williams

### Assignment 5 Requirements:

1.  Include splash screen with app title and list of articles. 
2.	Must find and use your own RSS feed. 
3.	Must add background color(s) or theme 
4.	Create and display launcher icon image 


README.md file should include the following items:        

1. Course title, your name, assignment requirements, as per A1
2. Screenshot of the item activity
3. Screenshot of running items activity
4. Screenshot of running the respecitive news article
                                                                                                                           

|Assignment Screenshots: | | 
|:-- |:-- |:--
| *Screenshot of items activity*: | *Screenshot of item activity*: | *Screenshot of news article*:
| | | 
| ![Items Activity](../img/a5_1.png) | ![Item Activity](../img/a5_2.png) | ![News Article](../img/a5_3.png) |
|

