> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advance Mobile Application Development

## Vincent Williams

### Assignment 3 Requirements:

1. Use a RadioBUtton and a RadioGroup to create conversion options
2. Must use toast to display results and errors
3. Must add background color(s) or theme 
4. Must create and display launcher icon image 

README.md file should include the following items:        

1. Course title, your name, assignment requirements, as per A1
2. Screenshot of running application�s unpopulated user interface
3. Screenshot of running application�s toast notification
4. Screenshot of running application�s converted currency user interface
                                                                                                                           

|Assignment Screenshots: | | 
|:-- |:-- |:--
| *Screenshot of Currency Converting App*: | *Screenshot of invalid Input*: | *My Tip Calculator with its fields populated*:
| | | 
| ![Screenshot of the Currency Converting App](../img/screen_1.png) | ![Screenshot of No input in the app](../img/screen_2.png) | ![Screenshot of $100 being converted to Euros](../img/screen_3.png) |
|

