> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advance Mobile Application Development

## Vincent Williams

### Project 2 Requirements:

1. Create Splash Screen
2. Insert 5 task into database
3. Background color
4. Create launcher icon

README.md file should include the following items:        

* Screenshot of running app's tasklist
                                                                                                                           

Assignment Screenshots: 

*Screenshot of Tasklist*: ![Tasklist](../img/P2.png) 

