> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advance Mobile Application Development

## Vincent Williams

### Assignment 2 Requirements:

1. Drop-down menu for total number of guests (including yourself)
2. Drop-down menu for tip percentage (5% increments)
3. Must add background color(s) or theme 
4. Must create and display launcher icon image 

README.md file should include the following items:        

* Screenshot of running application’s unpopulated user interface
* Screenshot of running application’s populated user interface
                                                                                                                           

|Assignment Screenshots: |
|:-- |:--
| *Screenshot of running My Tip Calculator*: | *My Tip Calculator with its fields populated*:
| |
| ![JDK Installation Screenshot](../img/cal_fill.png) | ![Android Studio Installation Screenshot](../img/cal_home.png)
| |

