
DROP DATABASE IF EXISTS vew17c;
CREATE DATABASE IF NOT EXISTS vew17c;

USE vew17c;

DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company(
  cmp_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  cmp_type ENUM('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership') NOT NULL,
  cmp_street VARCHAR(30) NOT NULL,
  cmp_city VARCHAR(20) NOT NULL,
  cmp_state CHAR(2) NOT NULL,
  cmp_zip INT(9) ZEROFILL UNSIGNED NOT NULL,
  cmp_phone BIGINT UNSIGNED NOT NULL,
  cmp_ytd_sales DECIMAL(8,2) UNSIGNED NOT NULL,
  cmp_url VARCHAR(200) NULL,
  cmp_notes VARCHAR(255) NULL,
  PRIMARY KEY (cmp_id)
);

DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer (
  cus_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  cmp_id TINYINT UNSIGNED NOT NULL,
  cus_ssn INT UNSIGNED NOT NULL,
  cus_fname VARCHAR(15) NOT NULL,
  cus_lname VARCHAR(30) NOT NULL,
  cus_street VARCHAR(30) NOT NULL,
  cus_city VARCHAR(20) NOT NULL,
  cus_state CHAR(2) NOT NULL,
  cus_zip INT UNSIGNED NOT NULL,
  cus_phone BIGINT UNSIGNED NOT NULL,
  cus_email VARCHAR(200) NULL,
  cus_balance DECIMAL(8,2) UNSIGNED NOT NULL,
  cus_tot_sales DECIMAL(8,2) UNSIGNED NOT NULL,
  cus_notes VARCHAR(255) NULL,
  PRIMARY KEY (cus_id),
  CONSTRAINT fk_company_customer
   FOREIGN KEY (cmp_id)
   REFERENCES company (cmp_id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT
);

CREATE UNIQUE INDEX ux_cus_ssn on customer(cus_ssn);

INSERT INTO company (cmp_type, cmp_street, cmp_city, cmp_state, cmp_zip, cmp_phone, cmp_ytd_sales, cmp_url, cmp_notes)
  VALUES 
  ('Non-Profit-Corp','1115 Easterwood Dr.','tallahassee','fl',32311,8508786180,85069.59,'www.americancross.com',NULL),
  ('C-Corp','10485 History Rd.','tallahassee','fl',32311,8502786989,85069.59,'www.atlus.com',NULL),
  ('S-Corp','11000 Tung Grove Rd.','tallahassee','fl',32311,8503786687,85069.59,'www.aniplex.com',NULL),
  ('LLC','8607 Highlander Dr.','tallahassee','fl',32311,8505786682,85069.59,'www.bushiroadna.com',NULL),
  ('Partnership','10002 Easterwood Dr','tallahassee','fl',32311,8509786381,85069.59,'www.gumius.com',NULL);

INSERT INTO customer (cmp_id, cus_ssn, cus_fname, cus_lname, cus_street, cus_city, cus_state, cus_zip, cus_phone, cus_email, cus_balance, cus_tot_sales, cus_notes)
  VALUES  
  (1,458658875,'Peter','Andersen','5987 Calloway St','tallahassee','fl',32324,8505478996,'pandersen@gmail.com',5000.00,37000.00,NULL),
  (2,214567889,'Calvin','Wilson','8856 North Monroe St','tallahassee','fl',32356,8502144447,'cwillson@gmail.com',5000.00,37000.00,NULL),
  (3,215469663,'Reese','Hill','0179 Dayflower Circle','tallahassee','fl',32311,8506327789,'rhill@gmail.com',5000.00,37000.00,NULL),
  (4,214578999,'Angel','Wintersen','2654 Thomasville Rd','tallahassee','fl',32388,8501243658,'awintersen@gmail.com',5000.00,37000.00,NULL),
  (5,458741223,'Caroline','Parker','2689 Shamrock Rd','tallahassee','fl',32302,8507418963,'cparker@gmail.com',5000.00,37000.00,NULL);


-- TEE C:\Users\Darkj\Desktop\Tee_Output\a2_report_vincnet.txt

SELECT * FROM company;
SELECT * FROM customer;

-- NOTEE


