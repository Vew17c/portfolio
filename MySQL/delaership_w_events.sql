-- Create inventory table: 

drop table if exists inventory; create table inventory  (   invent_id int (4) not null auto_increment,    item_desc varchar(30),    notes varchar(100),   primary key (invent_id) );  
 
-- Create log table: drop table if exists log; 

create table log  (   user varchar(30),   comment varchar(50),   mod_time timestamp );  
 
-- Create trigger: 

drop trigger if exists trg_inventory_mod; 
 
-- temporarily redefine delimiter 

delimiter // 

create trigger trg_inventory_mod   
AFTER INSERT on inventory   
FOR EACH ROW   
BEGIN   
 INSERT into log (user, comment, mod_time)   
 values (current_user(), "record added", now());   
END // 

delimiter ; 
 
-- Add new inventory records: 

insert into inventory(invent_id, item_desc, notes) values    (null, "broom", "first item stocked"),   (null, "vacuum", "second item stocked"),   (null, "jacuzzi", "third item stocked"),   (null, "lounge chair", "fourth item stocked"),   (null, "umbrella", "fifth item stocked"); 
 
drop trigger if exists trg_inventory_mod; 
 
select * from log; 


CREATE
    [DEFINER = { user | CURRENT_USER }]
    EVENT
    [IF NOT EXISTS]
    event_name
    ON SCHEDULE schedule
    [ON COMPLETION [NOT] PRESERVE]
    [ENABLE | DISABLE | DISABLE ON SLAVE]
    [COMMENT 'string']
    DO event_body;

schedule:
    AT timestamp [+ INTERVAL interval] ...
  | EVERY interval
    [STARTS timestamp [+ INTERVAL interval] ...]
    [ENDS timestamp [+ INTERVAL interval] ...]

interval:
    quantity {YEAR | QUARTER | MONTH | DAY | HOUR | MINUTE |
              WEEK | SECOND | YEAR_MONTH | DAY_HOUR | DAY_MINUTE |
              DAY_SECOND | HOUR_MINUTE | HOUR_SECOND | MINUTE_SECOND}