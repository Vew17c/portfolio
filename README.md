# Vincent Williams' Portflio

##Andorid Studios
*Listing of app thats I created in Android Studios during my time at FSU. Most of the app created have basic functionality, but a couple of the aps have more advance functionality, like using a database or pulling RSS feeds.*

[See More](./Android_Studios/README.md "Andorid Studio Files")

##Java
*List of code that I worte during my time at FSU. The codes do various thing, ranging from basic arithmetic, to using array, to creating text files on the user's computer. In addition, there are a couple that I created that uses Java's GUI features*

[See Files](./Java/ "Java Files")

##Webpages Files
*Two versions of my college portfolio done in two different languages: JSP and PHP. Below are the files for each version of my other portfolio.*

[JSP Webpage Files](./JSP/README.md "JSP Files")

[PHP Webpage Files](./PHP/README.md "PHP Files")

##SQL
*DescriptionList of SQL code that I have written during my time at FSU. The databases in htis protfolio varry on size and the amount of tables in each database. Furthermore, that are differnet types of database files in this protfolio.*

[See Files](./SQL/ "SQL Files")

