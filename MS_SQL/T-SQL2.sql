use master;
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'vew17c')
DROP DATABASE vew17c;
GO

(SELECT name FROM master.dbo.sysdatabases WHERE name = N'vew17c')
CREATE DATABASE vew17c;
GO

use vew17c;
GO
-----------------------------------------------------------------------------------

IF OBJECT_ID(N'dbo.person', N'U') IS NOT NULL
DROP TABLE dbo.person;

CREATE TABLE dbo.person(
  per_id SMALLINT NOT NULL ,
  per_ssn BINARY(64) NULL,
  per_fname VARCHAR(15) NOT NULL,
  per_lname VARCHAR(30) NOT NULL,
  per_gender CHAR(1) NOT NULL CHECK (per_gender = 'f' OR per_gender = 'm'),
  per_street VARCHAR(30) NOT NULL,
  per_city VARCHAR(30) NOT NULL,
  per_state CHAR(2) NOT NULL,
  per_zip INT NOT NULL CHECK (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  per_email VARCHAR(100) NOT NULL,
  per_dob DATE NOT NULL,
  per_type CHAR(1) NOT NULL CHECK (per_type = 'c' OR per_type = 's'),
  per_notes VARCHAR(255) NULL,
  PRIMARY KEY (per_id)
);

IF OBJECT_ID(N'dbo.region', N'U') IS NOT NULL 
DROP TABLE dbo.region;  

CREATE TABLE dbo.region(
  reg_id TINYINT NOT NULL ,
  reg_name CHAR(1) NOT NULL,
  reg_notes VARCHAR(255) NULL,
  PRIMARY KEY (reg_id)
);

IF OBJECT_ID(N'dbo.[state]', N'U') IS NOT NULL 
DROP TABLE dbo.[state];

CREATE TABLE dbo.[state](
  ste_id TINYINT NOT NULL ,
  reg_id TINYINT NOT NULL,
  ste_name CHAR(2) NOT NULL,
  ste_notes VARCHAR(255) NULL,
  PRIMARY KEY (ste_id),
  CONSTRAINT reg_id_state_fk
   FOREIGN KEY (reg_id)
   REFERENCES region(reg_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.city', N'U') IS NOT NULL 
DROP TABLE dbo.city;

CREATE TABLE dbo.city(
  cty_id SMALLINT NOT NULL,
  ste_id TINYINT NOT NULL,
  cty_name VARCHAR(30) NOT NULL,
  ste_notes VARCHAR(255) NULL,
  PRIMARY KEY (cty_id),
  CONSTRAINT ste_id_city_fk
   FOREIGN KEY (ste_id)
   REFERENCES [state](ste_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.store', N'U') IS NOT NULL 
DROP TABLE dbo.store;  

CREATE TABLE dbo.store(
  str_id SMALLINT NOT NULL ,
  cty_id SMALLINT NOT NULL,
  str_name VARCHAR(45) NOT NULL,
  str_street VARCHAR(30) NOT NULL,
  str_zip INT NOT NULL CHECK ([str_zip] like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  str_phone BIGINT NOT NULL CHECK ([str_phone] like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  str_email VARCHAR(100) NOT NULL,
  str_url VARCHAR(100) NOT NULL,
  str_notes VARCHAR(255) NULL,
  PRIMARY KEY (str_id),
  CONSTRAINT cty_id_store_fk
   FOREIGN KEY (cty_id)
   REFERENCES city(cty_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.vendor', N'U') IS NOT NULL 
DROP TABLE dbo.vendor;  

CREATE TABLE dbo.vendor(
  ven_id SMALLINT NOT NULL ,
  ven_name VARCHAR(45) NOT NULL,
  ven_street VARCHAR(30) NOT NULL,
  ven_city VARCHAR(30) NOT NULL,
  ven_state CHAR(2) NOT NULL,
  ven_zip INT NOT NULL CHECK (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') ,
  ven_phone BIGINT NOT NULL CHECK (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  ven_email VARCHAR(100) NOT NULL,
  ven_url VARCHAR(100) NOT NULL,
  ven_notes VARCHAR(255) NULL,
  PRIMARY KEY (ven_id)
);

IF OBJECT_ID(N'dbo.phone', N'U') IS NOT NULL 
DROP TABLE dbo.phone;  

CREATE TABLE dbo.phone(
  phn_id SMALLINT NOT NULL ,
  per_id SMALLINT NOT NULL,
  phn_num BIGINT NOT NULL CHECK (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  phn_type VARCHAR(10) NOT NULL CHECK (phn_type = 'h' OR phn_type = 'o' OR phn_type = 'c'),
  phn_notes VARCHAR(255) NULL,
  PRIMARY KEY (phn_id),
  CONSTRAINT per_id_phone_fk
   FOREIGN KEY (per_id)
   REFERENCES person(per_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.customer', N'U') IS NOT NULL 
DROP TABLE dbo.customer;  

CREATE TABLE dbo.customer(
  per_id SMALLINT NOT NULL ,
  cus_balance DECIMAL(7,2) NOT NULL,
  cus_total_sales DECIMAL(7,2) NOT NULL,
  cus_notes VARCHAR(255) NULL,
  PRIMARY KEY (per_id),
  CONSTRAINT per_id_customer_fk
   FOREIGN KEY (per_id)
   REFERENCES person(per_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.slsrep', N'U') IS NOT NULL 
DROP TABLE dbo.slsrep;  

CREATE TABLE dbo.slsrep(
  per_id SMALLINT NOT NULL ,
  srp_yr_sales_goal DECIMAL(8,2) NOT NULL,
  srp_ytd_sales DECIMAL(8,2) NOT NULL,
  srp_ytd_comm DECIMAL(7,2) NOT NULL,
  srp_notes VARCHAR(255) NULL,
  PRIMARY KEY (per_id),
  CONSTRAINT per_id_slsrep_fk
   FOREIGN KEY (per_id)
   REFERENCES person(per_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.srp_hist', N'U') IS NOT NULL 
DROP TABLE dbo.srp_hist;  

CREATE TABLE dbo.srp_hist(
  sht_id SMALLINT NOT NULL ,
  per_id SMALLINT NOT NULL,
  sht_type CHAR(1) NOT NULL CHECK (sht_type = 'i' OR sht_type = 'u' OR sht_type = 'd'),
  sht_modified TIMESTAMP NOT NULL,
  sht_modifier VARCHAR(45) NOT NULL,
  sht_date DATE NOT NULL,
  sht_yr_sales_goal DECIMAL(8,2) NOT NULL,
  sht_yr_sales_sales DECIMAL(8,2) NOT NULL,
  sht_yr_sales_comm DECIMAL(7,2) NOT NULL,
  sht_notes VARCHAR(255) NULL,
  PRIMARY KEY (sht_id),
  CONSTRAINT per_id_srp_hist_fk
   FOREIGN KEY (per_id)
   REFERENCES slsrep(per_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.contact', N'U') IS NOT NULL 
DROP TABLE dbo.contact;  

CREATE TABLE dbo.contact(
    cnt_id INT NOT NULL ,
    per_cid SMALLINT NOT NULL,
    per_sid SMALLINT NOT NULL,
    cnt_date DATETIME NOT NULL,
    cnt_notes VARCHAR(255) NULL,
    PRIMARY KEY (cnt_id),
  CONSTRAINT per_cid_contact_fk
   FOREIGN KEY (per_cid)
   REFERENCES customer(per_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE,
  CONSTRAINT per_sid_contact_fk
   FOREIGN KEY (per_sid)
   REFERENCES slsrep(per_id)
   ON DELETE NO ACTION
   ON UPDATE NO ACTION
);

IF OBJECT_ID(N'dbo.[order]', N'U') IS NOT NULL 
DROP TABLE dbo.[order];  

CREATE TABLE dbo.[order](
    ord_id INT NOT NULL ,
    cnt_id INT NOT NULL,
    ord_placed_date DATETIME NOT NULL,
    ord_filled_date DATETIME NOT NULL,
    ord_notes VARCHAR(255) NULL,
    PRIMARY KEY (ord_id),
  CONSTRAINT cnt_id_order_fk
   FOREIGN KEY (cnt_id)
   REFERENCES contact(cnt_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.invoice', N'U') IS NOT NULL 
DROP TABLE dbo.invoice;  

CREATE TABLE dbo.invoice(
    inv_id INT NOT NULL ,
    ord_id INT NOT NULL,
    str_id SMALLINT NOT NULL,
    inv_filled_date DATETIME NOT NULL,
    inv_salary DECIMAL(8,2) NOT NULL,
    inv_paid BIT NOT NULL,
    inv_notes VARCHAR(255) NULL,
    PRIMARY KEY (inv_id),
  CONSTRAINT ord_id_invoice_fk
   FOREIGN KEY (ord_id)
   REFERENCES [order](ord_id),
  CONSTRAINT str_id_invoice_fk
   FOREIGN KEY (str_id)
   REFERENCES store(str_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.product', N'U') IS NOT NULL 
DROP TABLE dbo.product;  

CREATE TABLE dbo.product(
    pro_id SMALLINT NOT NULL ,
    ven_id SMALLINT NOT NULL,
    pro_name VARCHAR(30) NOT NULL,
    pro_descript VARCHAR(45) NOT NULL,
    pro_weight FLOAT NOT NULL,
    pro_qoh SMALLINT NOT NULL,
    pro_cost DECIMAL(7,2) NOT NULL,
    pro_price DECIMAL(7,2) NOT NULL,
    pro_discount DECIMAL(3,0) NOT NULL,
    pro_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id),
  CONSTRAINT ven_id_product_fk
   FOREIGN KEY (ven_id)
   REFERENCES vendor(ven_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.order_line', N'U') IS NOT NULL 
DROP TABLE dbo.order_line;  

CREATE TABLE dbo.order_line(
    oln_id INT NOT NULL ,
    ord_id INT NOT NULL,
    pro_id SMALLINT NOT NULL,
    oln_qty SMALLINT NOT NULL,
    oln_price DECIMAL(7,2) NOT NULL,
    oln_notes VARCHAR(255) NULL,
    PRIMARY KEY (oln_id),
  CONSTRAINT ord_id_order_line_fk
   FOREIGN KEY (ord_id)
   REFERENCES [order](ord_id),
  CONSTRAINT pro_id_order_line_fk
   FOREIGN KEY (pro_id)
   REFERENCES product(pro_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.payment', N'U') IS NOT NULL 
DROP TABLE dbo.payment;  

CREATE TABLE dbo.payment(
    pay_id INT NOT NULL ,
    inv_id INT NOT NULL,
    pay_date DATETIME NOT NULL,
    pay_amt DECIMAL(7,2) NOT NULL,
    pay_notes VARCHAR(255) NULL,
    PRIMARY KEY (pay_id),
  CONSTRAINT inv_id_payment_fk
   FOREIGN KEY (inv_id)
   REFERENCES invoice(inv_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.product_hist', N'U') IS NOT NULL 
DROP TABLE dbo.product_hist;  

CREATE TABLE dbo.product_hist(
    pht_id INT NOT NULL ,
    pro_id SMALLINT NOT NULL,
    pht_date DATETIME NOT NULL,
    pht_cost DECIMAL(7,2) NOT NULL,
    pht_price DECIMAL(7,2) NOT NULL,
    pht_discount DECIMAL(3,0) NOT NULL,
    pht_notes VARCHAR(255) NULL,
    PRIMARY KEY (pht_id),
  CONSTRAINT pro_id_product_hist_fk
   FOREIGN KEY (pro_id)
   REFERENCES product(pro_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);

IF OBJECT_ID(N'dbo.[time]', N'U') IS NOT NULL 
DROP TABLE dbo.[time];  

CREATE TABLE dbo.[time](
  tim_id INT NOT NULL ,
  tim_yr SMALLINT NOT NULL CHECK (tim_yr LIKE '[0-9][0-9]'),
  tim_qtr TINYINT NOT NULL,
  tim_month TINYINT NOT NULL,
  tim_week TINYINT NOT NULL,
  tim_day TINYINT NOT NULL,
  tim_time TIME NOT NULL,
  tim_notes VARCHAR(255) NULL,
  PRIMARY KEY (tim_id)
);

IF OBJECT_ID(N'dbo.sale', N'U') IS NOT NULL 
DROP TABLE dbo.sale;  

CREATE TABLE dbo.sale(
    pro_id SMALLINT NOT NULL ,
    str_id SMALLINT NOT NULL,
    cnt_id INT NOT NULL,
    tim_id INT NOT NULL,
	sal_qty SMALLINT NOT NULL,
    sal_price DECIMAL(8,2) NOT NULL,
    sal_total DECIMAL(8,2) NOT NULL,
    sal_notes VARCHAR(255) NULL,
  PRIMARY KEY (pro_id, str_id, cnt_id, tim_id),
  CONSTRAINT pro_id_sale_fk
   FOREIGN KEY (pro_id)
   REFERENCES product(pro_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE, 
  CONSTRAINT str_id_sale_fk
   FOREIGN KEY (str_id)
   REFERENCES store(str_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE, 
  CONSTRAINT cnt_id_sale_fk
   FOREIGN KEY (cnt_id)
   REFERENCES contact(cnt_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE, 
  CONSTRAINT tim_id_sale_fk
   FOREIGN KEY (tim_id)
   REFERENCES [time](tim_id)
   ON DELETE CASCADE
   ON UPDATE CASCADE
);


GO

BEGIN TRANSACTION;

USE vew17c;

INSERT INTO dbo.person
  VALUES 
  (1, HASHBYTES('SHA2_512', 'test'), 'Wilber', 'Fische','m','5749 Dayflower Circle', 'Tallahassee', 'FL',400232310, 'wfische@gmail.com', '1984-04-07', 'c', NULL),
  (2, HASHBYTES('SHA2_512', 'test2'), 'John', 'Travolta','m','7906 Bradford Street', 'Tallahassee', 'FL', 240232311, 'jtravolta@gmail.com', '1985-04-07', 'c', NULL),
  (3, HASHBYTES('SHA2_512', 'test3'), 'Andy', 'Griffin','m', '782 Spruce Street ', 'Tallahassee', 'FL', 340232312, 'agriffin@gmail.com', '1986-04-07', 'c', NULL),
  (4, HASHBYTES('SHA2_512', 'test4'), 'Luke', 'Skywalker','m', '8906 Center Ave. ', 'Tallahassee', 'FL', 840232313, 'lskywalker@gmail.com', '1987-04-07', 'c', NULL),
  (5, HASHBYTES('SHA2_512', 'test5'), 'Andrew', 'Jackson','m', '11 Princess Ave. ', 'Tallahassee', 'FL', 640232314, 'ajackson@gmail.com', '1988-04-07', 'c', NULL),
  (6, HASHBYTES('SHA2_512', 'test6'), 'Taylor', 'Hacker','f', '347 Lakeshore Rd. ', 'Tallahassee', 'FL', 740232315, 'thacker@gmail.com', '1989-04-07', 's', NULL),
  (7, HASHBYTES('SHA2_512', 'test7'), 'Connor', 'McGiven','m', '54 Clay Drive ', 'Tallahassee', 'FL', 240232316, 'cmcgiven@gmail.com', '1990-04-07', 's', NULL),
  (8, HASHBYTES('SHA2_512', 'test8'), 'Evan', 'Powell','m', '86 NE. Clay Street ', 'Tallahassee', 'FL', 140232317, 'epowell@gmail.com', '1991-04-07', 's', NULL),
  (9, HASHBYTES('SHA2_512', 'test9'), 'Vincent', 'Willson','m', '310 Proctor St. ', 'Tallahassee', 'FL', 340232318, 'vwillson@gmail.com', '1992-04-07', 's', NULL),
  (10, HASHBYTES('SHA2_512', 'test0'), 'Jeffery', 'Pattersen','m', '67 Penn Dr. ', 'Tallahassee', 'FL', 240232319, 'jpattersen@gmail.com', '1993-04-07', 's', NULL);

---------------------------------------

 INSERT INTO dbo.region
 VALUES
 (1,'A',NULL),
 (2,'B',NULL),
 (3,'C',NULL),
 (4,'D',NULL),
 (5,'E',NULL);

INSERT INTO dbo.[state]
 VALUES
 (1,1,'FL',NULL),
 (2,2,'FL',NULL),
 (3,3,'FL',NULL),
 (4,4,'FL',NULL),
 (5,5,'FL',NULL);

INSERT INTO dbo.city
 VALUES
 (1,1,'Tallahassee',NULL),
 (2,2,'Tallahassee',NULL),
 (3,3,'Tallahassee',NULL),
 (4,4,'Tallahassee',NULL),
 (5,5,'Tallahassee',NULL);

---------------------------------------

INSERT INTO dbo.store
 VALUES
 (1,1,'Whole Foods', '2820 Rivers Circle',475193314,8507894867,'wfoods@gmail.com','www.wholefood.com',NULL),
 (2,2,'Capitola Market', '3820 Rivers Circle',375093423,8506894966,'cmarket@gmail.com','www.capitolamarket.com',NULL),
 (3,3,'West Side Shop', '4820 Rivers Circle',275993562,8505894065,'wsideshop@gmail.com','www.westsideshop.com',NULL),
 (4,4,'The Shack', '5820 Rivers Circle',175893651,8504894164,'theshack@gmail.com','www.theshack.com',NULL),
 (5,5,'Loot Crate', '6820 Rivers Circle',175793720,8503894263,'lcrate@gmail.com','www.lootcrate.com',NULL);

INSERT INTO dbo.vendor
 VALUES
 (1, 'Branch Foods', '2820 Market St.','Tallahassee','fl',475193314,8507894867,'bfoods@gmail.com','www.branchfood.com',NULL),
 (2, 'Capitola Circle', '3820 Market St.','Tallahassee','fl',375093423,8506894966,'ccircle@gmail.com','www.capitolcircle.com',NULL),
 (3, 'Eastside Brnach', '4820 Market St.','Tallahassee','fl',275993562,8505894065,'eside@gmail.com','www.eastsidbranch.com',NULL),
 (4, 'New Times', '5820 Market St.','Tallahassee','fl',175893651,8504894164,'newtimes@gmail.com','www.newtimes.com',NULL),
 (5, 'Pirates Bay', '6820 Market St.','Tallahassee','fl',175793720,8503894263,'pbay@gmail.com','www.piratesbay.com',NULL);

INSERT INTO dbo.phone 
 VALUES
 (1,1,'6125457182','o',NULL),
 (2,2,'8125457182','c',NULL),
 (3,3,'1125457182','h',NULL),
 (4,4,'6125457182','o',NULL),
 (5,5,'8125457182','c',NULL),
 (6,6,'6125457182','o',NULL),
 (7,7,'8125457182','c',NULL),
 (8,8,'1125457182','h',NULL),
 (9,9,'6125457182','o',NULL),
 (10,10,'8125457182','c',NULL);

INSERT INTO dbo.customer 
 VALUES
 (1,5000.97, 2900.37, NULL),
 (2,4000.97, 1900.37, NULL),
 (3,3000.97, 900.37, NULL),
 (4,2000.97, 4900.37, NULL),
 (5,1000.97, 5900.37, NULL);

INSERT INTO dbo.slsrep 
 VALUES
 (6, 80000.99,50000.97,1000.37,NULL),
 (7, 80000.99,40000.97,2000.37,NULL),
 (8, 80000.99,30000.97,3000.37,NULL),
 (9, 80000.99,20000.97,4000.37,NULL),
 (10, 80000.99,10000.97,5000.37,NULL);

INSERT INTO dbo.srp_hist (sht_id, per_id, sht_type, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_sales_sales, sht_yr_sales_comm,sht_notes)
 VALUES
 (1,6,'i','User_Name',GETDATE(),80000.99,50000.97,1000.37,NULL),
 (2,7,'i','User_Name',GETDATE(),80000.99,40000.97,2000.37,NULL),
 (3,8,'i','User_Name',GETDATE(),80000.99,30000.97,3000.37,NULL),
 (4,9,'i','User_Name',GETDATE(),80000.99,20000.97,4000.37,NULL),
 (5,10,'i','User_Name',GETDATE(),80000.99,10000.97,5000.37,NULL);

INSERT INTO dbo.contact 
 VALUES
 (1,1,6,GETDATE(),NULL),
 (2,2,7,GETDATE(),NULL),
 (3,3,8,GETDATE(),NULL),
 (4,4,9,GETDATE(),NULL),
 (5,5,10,GETDATE(),NULL);

INSERT INTO dbo.[order]
 VALUES
 (1,1,GETDATE(),GETDATE(),NULL),
 (2,2,GETDATE(),GETDATE(),NULL),
 (3,3,GETDATE(),GETDATE(),NULL),
 (4,4,GETDATE(),GETDATE(),NULL),
 (5,5,GETDATE(),GETDATE(),NULL);

INSERT INTO dbo.invoice 
 VALUES
 (1,1,1,GETDATE(),50000.99,0,NULL),
 (2,2,2,GETDATE(),60000.99,0,NULL),
 (3,3,3,GETDATE(),40000.99,1,NULL),
 (4,4,4,GETDATE(),30000.99,0,NULL),
 (5,5,5,GETDATE(),20000.99,1,NULL);

 INSERT INTO dbo.product 
 VALUES
 (1,1,'apples','fresh apples',0.98,50,0.50,0.75,5,NULL),
 (2,2,'bannanas','fresh bananas',0.98,50,0.50,0.75,5,NULL),
 (3,3,'grapes','fresh grapes',0.98,50,0.50,0.75,5,NULL),
 (4,4,'pears','fresh pears',0.98,50,0.50,0.75,5,NULL),
 (5,5,'butter lettuce ','locally grown butter letuce',0.98,50,0.50,0.75,5,NULL);

INSERT INTO dbo.order_line
 VALUES
 (1,1,1,50,500.00,NULL),
 (2,2,2,50,400.00,NULL),
 (3,3,3,50,600.00,NULL),
 (4,4,4,50,450.00,NULL),
 (5,5,5,50,300.00,NULL);

INSERT INTO dbo.payment 
 VALUES
 (1,1,GETDATE(),500.00,NULL),
 (2,2,GETDATE(),400.00,NULL),
 (3,3,GETDATE(),600.00,NULL),
 (4,4,GETDATE(),450.00,NULL),
 (5,5,GETDATE(),300.00,NULL);

INSERT INTO dbo.product_hist 
 VALUES
 (1,1,GETDATE(),0.50,0.75,5,NULL),
 (2,2,GETDATE(),0.50,0.75,5,NULL),
 (3,3,GETDATE(),0.50,0.75,5,NULL),
 (4,4,GETDATE(),0.50,0.75,5,NULL),
 (5,5,GETDATE(),0.50,0.75,5,NULL);

---------------------------------------

INSERT INTO dbo.time
 VALUES
 (1,12,50,01,1,01,CURRENT_TIMESTAMP, NULL),
 (2,12,50,01,1,02,CURRENT_TIMESTAMP, NULL),
 (3,12,50,01,1,03,CURRENT_TIMESTAMP, NULL),
 (4,12,50,01,1,04,CURRENT_TIMESTAMP, NULL),
 (5,12,50,01,1,05,CURRENT_TIMESTAMP, NULL);

INSERT INTO dbo.sale
 VALUES 
 (1,2,1,5,200,0.50,900.00,NULL),
 (2,4,3,5,100,0.75,200.00,NULL),
 (3,2,5,5,300,1.00,500.00,NULL),
 (4,4,3,5,250,1.25,300.00,NULL),
 (5,2,1,5,150,1.50,700.00,NULL),
 (1,4,3,4,200,1.75,900.00,NULL),
 (2,2,5,4,100,2.00,200.00,NULL),
 (3,4,3,4,300,2.25,500.00,NULL),
 (4,2,1,4,250,2.50,300.00,NULL),
 (5,4,3,4,150,2.75,700.00,NULL),
 (1,2,5,3,200,3.00,900.00,NULL),
 (2,4,3,3,100,3.25,200.00,NULL),
 (3,2,1,3,300,3.50,500.00,NULL),
 (4,4,3,3,250,3.75,300.00,NULL),
 (5,2,5,3,150,4.00,700.00,NULL),
 (1,4,3,2,200,4.25,900.00,NULL),
 (2,2,1,2,100,4.50,200.00,NULL),
 (3,4,3,2,300,4.75,500.00,NULL),
 (4,2,5,2,250,5.00,300.00,NULL),
 (5,4,3,2,150,5.25,700.00,NULL),
 (1,2,1,1,200,5.50,900.00,NULL),
 (2,4,3,1,100,5.75,200.00,NULL),
 (3,2,5,1,300,6.00,500.00,NULL),
 (4,4,3,1,250,6.25,300.00,NULL),
 (5,2,1,1,150,6.50,700.00,NULL);

COMMIT;


SELECT * FROM dbo.person;
SELECT * FROM dbo.store;
SELECT * FROM dbo.vendor;
SELECT * FROM dbo.phone;
SELECT * FROM dbo.customer;
SELECT * FROM dbo.slsrep;
SELECT * FROM dbo.srp_hist;
SELECT * FROM dbo.contact; 
SELECT * FROM dbo.[order];
SELECT * FROM dbo.invoice;
SELECT * FROM dbo.product;
SELECT * FROM dbo.order_line;
SELECT * FROM dbo.payment;
SELECT * FROM dbo.product_hist;
-----------------------------------------
SELECT * FROM dbo.city;
SELECT * FROM dbo.[state];
SELECT * FROM dbo.region;
SELECT * FROM dbo.sale;
SELECT * FROM dbo.time;
