import java.util.Scanner;
import java.util.Random;
import java.lang.Math.*;

public class Circle {
    public static Double NumberEvlauater( String evalN ){
        Double ReturnEval = 0.0, 
        LoopB = 1.0;

        Scanner input = new Scanner (System.in);

        do {
            try {  // Test for Characters
                ReturnEval = Double.parseDouble(evalN);
                LoopB = 0.0;
            } catch (NumberFormatException e) { 
                System.out.print("Not a valid number!" + "\n" + "\n");
                System.out.print("Please try again. Enter circle radius: ");
                    evalN = input.nextLine(); 
            }
        } while ( LoopB == 1.0 );

        return ReturnEval;
    }

    public static void main (String [] args) {
        String Numb = "";
        Double Entry = 0.0,
              Dia = 0.0,
              Cir = 0.0,
              Are = 0.0;
        Scanner input = new Scanner (System.in);

        System.out.println("Program calculates the diameter, curcumfrence, and circle area.");
        System.out.println();

        System.out.print("Enter Circle radius: ");
        Numb = input.nextLine();
        Entry=NumberEvlauater(Numb);

        Dia = ( 2 * Entry );
        Cir = ( Dia * Math.PI );
        Are = ( ( Entry * Entry ) * Math.PI );

        System.out.print("\n");


        System.out.print("Circle Diameter: "); 
        System.out.printf("%.2f", Dia); 
        System.out.print("\n");


        System.out.print("Circumfrence: "); System.out.printf("%.2f", Cir); System.out.print("\n");
        System.out.print("Area: "); System.out.printf("%.2f", Are); System.out.print("\n");
    }
}