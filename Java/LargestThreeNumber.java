import java.util.Scanner;
import java.util.Random;

public class LargestThreeNumber {
    public static double NumberEvlauater( String evalN ){
        double ReturnEval = 0, 
        LoopB = 1;

        Scanner input = new Scanner (System.in);

        do {
            try {  // Test for Characters
                ReturnEval = Double.parseDouble(evalN);
                if (ReturnEval == Math.floor(ReturnEval)) { 
                    LoopB = 0; // Breaks out of the Loop
                } else {
                    System.out.print ("Invalid Number, please enter a valid number: ");
                    evalN = input.nextLine(); 
                }
            } catch (NumberFormatException e) { 
                System.out.print ("Not a number, please enter a number: ");
                    evalN = input.nextLine(); 
            }
        } while ( LoopB == 1 );

        return ReturnEval;
    }

    public static void main (String [] args) {
        String Numb = "";
        double numb[] = new double[3];
        double maxNumb = 0.0;
        int i = 0;
        Scanner input = new Scanner (System.in);

        System.out.println("Program evaluates largest of three integers.");
        System.out.println();

        System.out.print("Please enter first number: ");
        Numb = input.nextLine();
        numb[0]=NumberEvlauater(Numb);

        System.out.print("Please enter second number: ");
        Numb = input.nextLine();
        numb[1]=NumberEvlauater(Numb);

        System.out.print("Please enter third number: ");
        Numb = input.nextLine();
        numb[2]=NumberEvlauater(Numb);

        maxNumb = numb[0];

        for ( i = 0; i < 3; i++){
            if (maxNumb < numb[i]){
                maxNumb = numb[i];
            }
        }

        System.out.printf("\nThe largest number is: %.0f", maxNumb);
        
    }
}