import java.util.Scanner;

public class StringCount {
    public static void main (String[] args){
        String testString;
        char[] testarray;
        int    resLetter = 0,
               resSpace = 0,
               resNumber = 0,
               resOther = 0;

        Scanner input = new Scanner(System.in);

        System.out.print("Please enter string: ");
        testString = input.nextLine();
        testarray = testString.toCharArray();

        for(int i = 0; i < testString.length(); i++){
			if(Character.isLetter(testarray[i])){
				resLetter ++ ;
			}
			else if(Character.isDigit(testarray[i])){
				resNumber ++ ;
			}
			else if(Character.isSpaceChar(testarray[i])){
				resSpace ++ ;
			}
			else{
				resOther ++;
			}
        }
        
        System.out.print("\n\n");
        System.out.println("Your string: \"" + testString + "\" has the following number and types of characters:");
        System.out.println("letter(s): " + resLetter);
        System.out.println("space(s): " + resSpace);
        System.out.println("number(s) " + resNumber);
        System.out.println("other character(s): " + resOther);
    }
}