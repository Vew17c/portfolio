import java.util.Scanner; //needed to use Scanner for input
// Made by: Vincnet Williams
public class Pandora {
  public static void main(String[] args) {
   
        //Declare variables
        int menuChoice = 0,
            menuScrene = 0,
            musicChoice = 0;
        String lname = "",
               channelName = "",
               channelNew = "";
     
        //Create a Scanner object
        Scanner input = new Scanner(System.in);
    
        //Display the Opening Statement which includes the Pandora Menu
        
        System.out.println("........................ Welcome to Pandora! ........................");
        System.out.println(".                                                                   .");
        System.out.println(".                          Pandora Menu:                            .");
        System.out.println(".                     1. New Pandora Channel                        .");
        System.out.println(".                     2. Play Pandora Channel                       .");
        System.out.println(".                     3. Exit the menu                              .");
        System.out.println(".                                                                   .");
        System.out.println(".....................................................................");
  
        //Prompt the user for their last name and menu choice option
        System.out.print ("Hello user! Please enter your last name: ");
        lname = input.nextLine();
        
        System.out.print("Please choose one of the options: (Press 1 to create a New Pandora Channel, Press 2 to Play Pandora Channel, Press 3 to Exit the menu: ");
        menuChoice = input.nextInt();
      
  
        //Convert last name to uppercase
        lname = lname.toUpperCase(); 
  
        //Control statement (if()/else if() or switch()) that is based on the user's menu choice
                //process the user's menu choice (options:  1, 2, 3, other)
        switch (menuChoice){
          
          case 1: {System.out.println("You have chosen the option of: Create New Pandora Channel");
                  System.out.print("Please enter the name you want the new music channel to be called: ");
                  
                  input.nextLine();
                  channelNew = input.nextLine();
                   
                  System.out.println("You have created the new music channel " + channelNew.toUpperCase());
                  break;
          }
          case 2: {System.out.println("You have chosen the option of: Play Pandora Channel");
                  System.out.println(" 1. Live and Learn \n 2. Open your Heart \n 3. Don't Loose Your Way \n 4. Fire \n 5. Alchemy");
                  System.out.print("Please enter the number of the channel you wish to play: ");
                  
                  musicChoice = input.nextInt();
                  
                  if (musicChoice == 1){channelName = "Live and Learn";}
                    else if (musicChoice == 2){channelName = "Open your Heart";}
                    else if (musicChoice == 2){channelName = "Don't Loose Your Way";}
                    else if (musicChoice == 2){channelName = "Fire";}
                    else if (musicChoice == 2){channelName = "Alchemy";}
                    else {System.out.print("Invalid Choice. Please enter the number of the channel you wish to play: ");
                              musicChoice = input.nextInt();
                    }
                  
                  channelName = channelName.toUpperCase();
                  
                  System.out.println("You are now playing " + channelName);
                  break;
                  
            
          }
          case 3: {System.out.println("You have chosen the option of: Exit the menu");
                   break;
          }
          default: {System.out.println("Invalid Choice");
                    System.out.print("Please choose one of the options: (Press 1 to create a New Pandora Channel, Press 2 to Play Pandora Channel, Press 3 to Exit the menu");
                    menuChoice = input.nextInt();
                    break;
            }
                    
        }
  
        //Display Thank you message
        System.out.println("Thank you " + lname + " for using the Pandora app. \n Come back soon.");

  }//end of main
}//end of class