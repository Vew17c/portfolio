import java.util.Scanner;

public class ReadError{
    public static void main (String [] args) {
            String lname = "";
            int age;
            Scanner input = new Scanner(System.in);
            
            System.out.print("Please enter your age: ");
            
            age = input.nextInt();
            
            input.nextLine(); // FLUSH THE BUFFER REMOVING THE NEW LINE SYMBOL
            
            System.out.print("Please Enter Last name: ");
            
            lname = input.nextLine();
            
            System.out.println("Users last name is " +lname; "and age is: " + age);
            
    }// Main End.
}