import java.util.Scanner;

public class CharExample1 {
    public static void main (String [] args) {
        
        //Decalre Varibles
        char middleInt = 'l';
        char firstNameChar = 'C';
        char lastNameChar = 'c';
        
        System.out.println ("Middle Initial: " + Character.toUpperCase(middleInt));
        System.out.println ("Is First Name a Digit?: " + Character.isDigit(firstNameChar));
        System.out.println ("Is Last Name Char Uppercase: " + Character.isUpperCase(lastNameChar));
            
    }// Main End.
}