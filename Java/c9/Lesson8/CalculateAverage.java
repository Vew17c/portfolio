/* 

- Value Returning Methood
1. Modifier (public, private) (static)
2. Return type (int, ling, double, void, e.c.t.)
3. Name of methood
4. Parameter list
5. Methood body

- Void Methood
1. Modifier (public, private) (static)
2. Return type (int, ling, double, void, e.c.t.)
3. Name of methood
4. Parameter list
5. Methood body
*/
import java.util.Scanner;

public class CalculateAverage {
    public static void main (String [] args) {
        int num1 = 25,
            num2 = 19;
        double average;
            
            average = calculateAverage(num1, num2);
            
            calculateSum(0,0);
        
        System.out.println("The average is: " + average);
    }// Main End.
    
    //Function Defintion for Calculate Average
    public static double calculateAverage(int val1, int val2){
        double average;
        average = (val1+val2)/2;
        return average;
    }//end of Calculate Average Methood
    
    //FUnction Definiton for Calculating Sum
    public static void calculateSum(int val1, int val2){
        int sum;
        Scanner input = new Scanner(System.in);
        System.out.print("Type in Varible 1: ");
        val1 = input.nextInt();
        System.out.print("Type in Varible 2: ");
        val2 = input.nextInt();
        sum = val1+val2;
        System.out.println("The sum of the numbers is: " + sum);
    }
}