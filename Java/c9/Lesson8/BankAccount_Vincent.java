

public class BankAccount_Vincent {
    public static void main (String [] args) {
        // Varibles
        int accountNo = 0, //Stores account Number
            menu = 0;      //Varible that controls which menu state the program is in.
        double withdraw = 0, // Varible that stores withdraws
               balanceNo = 0,// Varubke that stores balance
               deposit = 0;  // Varible that stores the deposit ammount
        String letterChoice = "";
               
         // Creates input
        Scanner input = new Scanner (System.in);
        
        accountNo = accountGet(0); //Activates the function that gathers the account number.
        menu = 1;// Sets the menu to 1
        
        //Loop that controls the menu
        do {
            //A promp for valid answer choices
            System.out.print ("\nEnter D for Deposit, Enter W for Withdraw, Enter B for Balance, Enter X for Exit: "); 
            //A promp for valid answer choices
            letterChoice = input.next(); // Gets the answer choice
            // Moves menu to fit the option type
            switch (letterChoice) {
                case "W": menu = 2; break;
                case "B": menu = 3; break;
                case "D": menu = 4; break;
                case "X": menu = 5; break;
            }
            
            if (menu == 2){ balanceNo = withdrawMoney(balanceNo); menu = 1;}
            if (menu == 3){ displayBalance (accountNo, balanceNo); menu = 1;}
            if (menu == 4){ balanceNo = depositMoney(balanceNo); menu = 1;}
        
        } while (menu != 5);
        
        System.out.println("\nThank you for being a loyal Seminole Bank Customer!");
        
    }// Main End.
    
    //Function Defines the Account Number Gathering Process
    public static int accountGet(int num) {
        // Creates the scanner
        Scanner input = new Scanner (System.in);
        
        // Propt for welcome and account number
        System.out.print("Welcome to Seminole Bank! \nPlease enter your 5-digit Seminole Account Number: ");
        num = input.nextInt();
        // Prompt for options
        System.out.println ("Thank You!");
        return num;
    }
    //Function Defines Money Withdraw
    public static double withdrawMoney(double bal){
        double withdraw;
        Scanner input = new Scanner (System.in);
        
        System.out.print("\nEnter the amount of the withdraw: "); //Prompts for withdraw amount
        withdraw = input.nextDouble(); //Gets withdraw ammount
        bal -= withdraw; //Subtracs it from balalnce
        return bal; //Returns balance to Main function
    }
    
    //Function Defines Account Balance
    public static void displayBalance(int accountNo, double balanceNo){
        System.out.println("\nAccount Number: " +accountNo + " has a balacne of: $" +balanceNo); //Displays the account No. and blalnce
    }
    
    //Function Defines Deposit Money
    public static double depositMoney(double bal){
        double deposit;
        Scanner input = new Scanner (System.in);
        
        System.out.print("\nEnter the amount to deposit: "); //Promts for the deposit amount
        deposit = input.nextDouble(); // Gets deposit amount
        bal += deposit; //Adds it t balance
        return bal; //Returns the balance to the Main Function
    }
}