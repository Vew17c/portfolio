import java.util.Scanner;

public class AreaEnter {
    public static void main (String [] args) {
        // Read in the circle's radius
        double rad, area;
        final double PI = 3.14159;
        
        Scanner radData = new Scanner(System.in);
        
        // Prompt to find get the radius
        System.out.print("Enter the radius you want to calculate: ");
        //Inputing the radius
        rad = radData.nextDouble();
        // Compute the area using the formula
        area = (rad * rad) * PI;
        //Display result on screne
        System.out.println ("The area is: " + area + "for the radius: " + rad);
    }// Main End.
}