// Including the documentation for the scanner class
import java.util.Scanner;

public class ReadAge {
    public static void main (String [] args) {
    
    // Declare varibles
        
    int age,
        newAge;
        
        //Declare and create a new (data) of tye Scanner - for recording data
        Scanner data = new Scanner(System.in);
        //Propt the user for their age
        System.out.print ("Please enter your age: ");
        //Read age form keyboard
         age = data.nextInt();
        //Compute new age
        newAge = age + 10;
        //Display the new age to screen (Console)
        System.out.println("Your age will be " +newAge + " in 10 years.");
    }// Main End.
}