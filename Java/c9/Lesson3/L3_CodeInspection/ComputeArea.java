public class ComputeArea {
  /** Main method */
  public static void main(String[] args) {
    double radius,
           area;
    
    // Assign a radius
    radius = 20;
    
    // Compute area
    area = radius * radius * 3.14159;
    
    // Display results
    System.out.println("The area for the circle of radius " +
      radius + " is " + area);
  } //End of Main Methood
}// End of the ClassS