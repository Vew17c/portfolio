
/*

*/

public class Area {
    public static void main (String [] args) {
        // Read in the circle's radius
        double rad = 4,
               area;
        final double PI = 3.14159;
        
        // Compute the area using the formula
        area = (rad * rad) * PI;
        
        //Display result on screne
        System.out.println ("The area is: " +area "for the radius: " +rad);
    }// Main End.
}