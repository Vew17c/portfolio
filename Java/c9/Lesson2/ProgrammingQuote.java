// Programmed By Vincent Williams
public class ProgrammingQuote {
    public static void main (String [] args) {
    // Actual Quote. Used \n to have the quote end on the current position and to continue writing on the next line.
    System.out.println ("\"Whether you want to uncover the secerets of the universe, \n or you wnt to pursue a career in the 21st century, basic \n compter programming is an essential skill to learn.\"");  
    System.out.println ("                                        - Stephen Hawking");
    }// Main End.
} // ProgrammingQuote End