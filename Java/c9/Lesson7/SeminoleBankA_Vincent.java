import java.util.Scanner;

// While Loop

public class SeminoleBankA_Vincent {
    public static void main (String [] args) {
        // Varibles
        int accountNo = 0, 
            menu = 0;
        double withdraw = 0,
               balanceNo = 0,
               deposit = 0;
        String letterChoice = "";
        
        // Creates input
        Scanner input = new Scanner (System.in);
        
        // Propt for welcome and account number
        System.out.print("Welcome to Seminole Bank! \nPlease enter your 5-digit Seminole Account Number: ");
        accountNo = input.nextInt();
        // Prompt for options
        System.out.println ("Thank You!");
        menu = 1;
        
        while (menu != 5) {
        System.out.print ("\nEnter D for Deposit, Enter W for Withdraw, Enter B for Balance, Enter X for Exit: ");
        letterChoice = input.next();
        // Moves menu to fit the option type
        switch (letterChoice) {
            case "W": menu = 2; break;
            case "B": menu = 3; break;
            case "D": menu = 4; break;
            case "X": menu = 5; break;
            }
            
        if (menu == 2){
           System.out.print("\nEnter the amount of the withdraw: "); 
           withdraw = input.nextDouble();
           balanceNo -= withdraw;
           menu = 1;
            }
        if (menu == 3){
            System.out.print("\nAccount Number: " +accountNo + " has a balacne of: $" +balanceNo);
            menu = 1;
            }
        if (menu == 4){
            System.out.print("\nEnter the amount to deposit: ");
            deposit = input.nextDouble();
            balanceNo += deposit;
            menu = 1;
            }
        }
        
        while (menu == 5){
            System.out.println("\nThank you for being a loyal Seminole Bank Customer!"); break;
        }
    }// Main End.
}