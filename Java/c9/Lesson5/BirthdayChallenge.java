import java.util.Scanner;

public class BirthdayChallenge {
    public static void main (String [] args) {
         //Declare varibles
         int day, month, year;
         String monthName = " ";
         
         //Create Scanner
         Scanner input = new Scanner(System.in);
         
         //Prompt the user for month they were born as an integer
         System.out.print ("Please enter the month you were born as an NUMBER: ");
         //Read Month Born
         month = input.nextInt();
         System.out.println (+month);
         
        while (month < 1 || month > 12) {
             System.out.print ("Invalid answer. Please enter the month you were born as an NUMBER: ");
             month = input.nextInt();
         }
        
         //Prompt the user for the day born
         System.out.print ("Please enter the day you were born: ");
         //Read the day born
         day = input.nextInt();
          while (day < 1 || day > 31) {
             System.out.print ("Invalid Number. Please enter the day you were born: ");
             month = input.nextInt();
         }
         
         //Prompt the user for the year they were born
          System.out.print ("Please enter the 4 digit year you were born: ");
         //Read the year born
         year = input.nextInt();
         
         while (year < 1 || year > 2017) {
             System.out.print ("Invalid Number. Please enter the 4 digit year you were born: ");
             year = input.nextInt();
         }
         
         //Process the month by associatng the month (int) to month (string)
         //Using If/Else statments
         
         if (month == 1) {monthName = "January" ; }
         else if (month == 2) {monthName = "Feburary" ; }
         else if (month == 3) {monthName = "March" ; }
         else if (month == 4) {monthName = "April" ; }
         else if (month == 5) {monthName = "May" ; }
         else if (month == 6) {monthName = "June" ; }
         else if (month == 7) {monthName = "July" ; }
         else if (month == 8) {monthName = "August" ; }
         else if (month == 9) {monthName = "September" ; }
         else if (month == 10) {monthName = "October" ; }
         else if (month == 11) {monthName = "November" ; }
         else if (month == 12) {monthName = "December" ; }
        
         //Display the formulated DoB
         System.out.print ("Your date of birth is: " + monthName);
         System.out.print (" " +day);
         System.out.println (", " +year);
    }// Main End.
}