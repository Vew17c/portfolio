/************************************************************************************************
 * File Name:   InvoiceCalculator.java
 * 
 * Due Date:    11 / 03 / 2017
 * 
 * Author:      Vincent Williams
 * 
 * Program Description:  
 *              Hello! This is a program written by me to calculate out the invoice amounts dued to the users after their membership type and subtotals are collected.  
 * 
 * Input:       The user will enter customer type (integer) and the subtotoal (double).
 *
 * Output:      Print the Invoice Report which includes the following: 
 *              -subtotal, customer type, discount percentage, discount amount, and total
 *
 * Processing:  Calculate the discount rate based on the customer type and subtotal using a control
 *              statement.  Also, calculate the invoice total using the equation described in the
 *              assignment description.
 * **********************************************************************************************/
 
 import java.util.Scanner;
 
public class InvoiceCalculator
{
    public static void main(String[] args)
    {
        int type; // The varible that
        double subTotal, 
               discount = -1, 
               discountTotal,
               invoiceTotal;
        Scanner input = new Scanner(System.in);
        
        //Display a message
        System.out.print ("Please enter the customer type (Enter 1 for Bronze, Enter 2 for Silver, Enter 3 for Gold, or Enter 4 for Platnium): ");
        
 
        //Prompt user for customer type
        type = input.nextInt();

 
        //Read customer type
        switch (type) {
            case 1: type = 1; break;
            case 2: type = 2; break;
            case 3: type = 3; break;
            case 4: type = 4; break;
            default: type = 1; break; 
        }
           
        //Prompt user for subtotal   
        System.out.print ("Please enter the subtotal: ");

        
        //Read subtotal
        subTotal = input.nextDouble();

        
        //Calculate Discount Rate (using a control statement)
        if (type == 1) { // Program goes through these discounts if type is 1
            if (subTotal >= 1000) {discount = .2;}
            if (subTotal < 1000 && subTotal >= 500) {discount = .15;}
            if (subTotal < 500 && subTotal >= 300) {discount = .10;}
            if (subTotal <300 ) {discount = .2;}
        } else if (type == 2) {// Program goes through this discount if type is 2
            discount = .3;
        } else if (type == 3) {// Program goes through these discounts if type is 3
            if (subTotal >= 5000) {discount = .6;}
            if (subTotal < 5000 && subTotal >= 2000) {discount = .5;} 
            if (subTotal < 2000) {discount = .4;}
        } else if (type == 4) {// Program goes through this discounts if type is 4
            discount = .6;
        }
            
        //Calculate the Discount Amount
        discountTotal = subTotal*discount;
        

        //Calculate Invoice Total
        invoiceTotal = subTotal - discountTotal;


        //Display thank you message
        System.out.println ("\nThank you for using the Seminole Invoice Total Calculator:");
        System.out.println ("INVOICE REPORT: \n");
        

        //Format and display the results
        System.out.println("Subtotal: " +subTotal);
        System.out.println("Customer Type: " +type);
        System.out.println("Discount Percent: " +discount);
        System.out.println("Discount Amount: " +discountTotal);
        System.out.println("Total: "+ invoiceTotal);

       
    }//end of main
}//end of InvoiceCalculator class