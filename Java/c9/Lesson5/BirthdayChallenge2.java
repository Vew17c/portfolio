import java.util.Scanner;

public class BirthdayChallenge2 {
    public static void main (String [] args) {
         //Declare varibles
         int day, month, year;
         String monthName = " ";
         
         //Create Scanner
         Scanner input = new Scanner(System.in);
         
         //Prompt the user for month they were born as an integer
         System.out.print ("Please enter the month you were born as an NUMBER: ");
         //Read Month Born
         month = input.nextInt();
         System.out.println (+month);
         
        while (month < 1 || month > 12) {
             System.out.print ("Invalid answer. Please enter the month you were born as an NUMBER: ");
             month = input.nextInt();
         }
        
         //Prompt the user for the day born
         System.out.print ("Please enter the day you were born: ");
         //Read the day born
         day = input.nextInt();
          while (day < 1 || day > 31) {
             System.out.print ("Invalid Number. Please enter the day you were born: ");
             month = input.nextInt();
         }
         
         //Prompt the user for the year they were born
          System.out.print ("Please enter the 4 digit year you were born: ");
         //Read the year born
         year = input.nextInt();
         
         while (year < 1 || year > 2017) {
             System.out.print ("Invalid Number. Please enter the 4 digit year you were born: ");
             year = input.nextInt();
         }
         
         //Process the month by associatng the month (int) to month (string)
         //Using If/Else statments
         
         switch (month) {
             case 1: monthName = "January" ; break;
             case 2: monthName = "Feburary" ; break;
             case 3: monthName = "March" ; break;
             case 4: monthName = "April" ; break;
             case 5: monthName = "May" ; break;
             case 6: monthName = "June" ; break;
             case 7: monthName = "July" ; break;
             case 8: monthName = "August" ; break;
             case 9: monthName = "September" ; break;
             case 10: monthName = "October" ; break;
             case 11: monthName = "November" ; break;
             case 12: monthName = "December" ; break;
         }
        
         //Display the formulated DoB
         System.out.print ("Your date of birth is: " + monthName);
         System.out.print (" " +day);
         System.out.println (", " +year);
    }// Main End.
}