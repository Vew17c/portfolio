import java.util.Scanner;

public class BirthdayChallenge3 {
    public static void main (String [] args) {
         //Declare varibles
         int day, month, year;
         String monthName = " ";
         
         //Create Scanner
         Scanner input = new Scanner(System.in);
         
         //Prompt the user for month they were born as a string
         System.out.print ("Please enter your birth month as a STRING.\nFor Example: January February, March, e.c.t): ");
         //Read Month Born
         monthName = input.next();
        
         //Prompt the user for the day born
         System.out.print ("Please enter the day of the month you were born: ");
         //Read the day born
         day = input.nextInt();
          while (day < 1 || day > 31) {
             System.out.print ("Invalid Number. Please enter the day of the month you were born:");
             month = input.nextInt();
         }
         
         //Prompt the user for the year they were born
          System.out.print ("Please enter the 4 digit year you were born: ");
         //Read the year born
         year = input.nextInt();
         
         while (year < 1000 || year > 10000) {
             System.out.print ("Invalid Number. Please enter the 4 digit year you were born: ");
             year = input.nextInt();
         }
         
         //Process the month by associatng the month (int) to month (string)
         //Using If/Else statments
         
         switch (monthName) {
             case "January": month = 1 ; break;
             case "Febuary": month = 2 ; break;
             case "March": month = 3 ; break;
             case "April": month = 4 ; break;
             case "May": month = 5 ; break;
             case "June": month = 6 ; break;
             case "July": month = 7 ; break;
             case "August": month = 8 ; break;
             case "September": month = 9 ; break;
             case "October": month = 10 ; break;
             case "November": month = 11 ; break;
             case "December": month = 12 ; break;
             default: System.out.print("Invalid input. Please enter your birth month as a string: "); 
             month = input.nextInt();
         }
        
         //Display the formulated DoB
         System.out.print ("Your date of birth is: " + month);
         System.out.print ("/" +day);
         System.out.println ("/" +year);
    }// Main End.
}