import java.util.Scanner;

public class PrintFExample {
    public static void main (String [] args) {
            
            // declare varibles
            double num1,
                   num2 = ;
            // Creates Scanner object
            Scanner input = new Scanner(System.in);
            
            //Promts the user for a float-point umner
            System.out.print("Please enter a float-point number: ");
            
            //Read the number from the keyboard
            num1 = input.nextDouble();
            
            //Print num1 without formatting.
            System.out.println("Num1 is: " +num1 " and Num2 is: " +num2);
            
            //Print num1 with formating displaying 3 digits after decimal point.
            System.out.printf("Num1 with 3 digits is: %.3f and num2 is: %.2f \n" , num1, num2);
            
    }// Main End.
}