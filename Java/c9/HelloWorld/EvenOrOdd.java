
import java.util.Scanner;

public class EvenOrOdd {
    public static void main (String [] args) {
        int numb;
        Scanner input = new Scanner (System.in);
        
        System.out.print ("Enter integer: ");
        numb = input.nextInt();
        
        if ((numb % 2) == 0){
            System.out.println ("The number is even.");
        } else {
            System.out.println ("The number is odd.");
        }
    }
}