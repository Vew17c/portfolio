import java.util.Scanner;

public class NumberSwap {

    public static void main (String[] args){
        int numb1 = 0,
            numb2 = 0,
            placeholder = 0,
            numbReturn = 0;

        double testNumb = 0.0;

        boolean lock = true;

        String line = "";

        Scanner input = new Scanner(System.in);

        System.out.print("\nNote: Program checks for integers and non-numeric values. \n\n");

        //Grabs the input for number 2. Using Try Catch to catch an user input a letter value (which is invalid).
        while (lock == true) { 
            System.out.print("Please enter first number: ");
            line = input.next();
        try {
            testNumb = Double.parseDouble(line);
            lock = false;
            if (testNumb%1 != 0) {
                System.out.print("Not a valid integer\n\n");
                System.out.print("Please try again. ");
                lock = true;
            } else {
                numb1 = (int)testNumb;
            }
        } catch (Exception e) {
            System.out.print("Not a valid integer\n\n");
            System.out.print("Please try again. ");
            lock = true;
        }
        }

        //Grabs the input for number 2. Using Try Catch to catch an user input a letter value (which is invalid).
        System.out.print("\n");
        lock = true;
        while (lock == true) { 
            System.out.print("Please enter second number number: ");
            line = input.next();
        try {
            testNumb = Double.parseDouble(line);
            lock = false;
            if (testNumb%1 != 0) {
                System.out.print("Not a valid integer\n\n");
                System.out.print("Please try again. ");
                lock = true;
            } else {
                numb2 = (int)testNumb;
            }
        } catch (Exception e) {
            System.out.print("Not a valid integer\n\n");
            System.out.print("Please try again. ");
            lock = true;
        }
        }

        //Prints to the console telling you what numbers are stored in numb1 and numb2 before the switch
        System.out.println("\nThe number's before the switch: ");
        System.out.println("Number 1: " + numb1);
        System.out.println("Number 2: " + numb2);

        //Switches the numbers around
        placeholder = numb1;
        numb1 = numb2;
        numb2 = placeholder;

        //Prints to the console telling you what numbers are stored in numb1 and numb2 after the switch
        System.out.println("\nThe number's after the switch: ");
        System.out.println("Number 1: " + numb1);
        System.out.println("Number 2: " + numb2);

    }
}